<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable=[
        'name', 'region_id', 'street_1','street_2','post_code','city','phone', 'order', 'place_ID', 'facebook_url', 'googleplus_url'
    ];

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function openingTimes()
    {
        return $this->hasMany('App\OpeningTime');
    }
}
