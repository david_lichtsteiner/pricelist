<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Requests;
use App\Service;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function update(Service $service){
        $field = Request::get('name');
        $service->$field = Request::get('value');
        $service->save();
    }

    public function store(Requests\RepairRequest $request){
        $input = Request::all();

        $input["order"] = $this->getOrder( $input["device_id"]);


        Service::create($input);

        return redirect('/device/'.Request::get('device_id'));

    }

    public function destroy(Service $service){
        $device_id = $service->device_id;

        $service->delete();

        return redirect('device/'.$device_id);
    }

    public function updateOrder(){
        $service = Request::all();
        $array = $service["service"];
        $size = count($array);
        for($i = 0; $i < $size; $i++){
            $id = $array[$i];
            $newOrder = $i;
            Service::find($id)->update(['order'=> $newOrder]);
        }
    }


    private function getOrder($deviceID){


        $repair = Service::where('device_id','=',$deviceID)->orderBy('order','desc')->get()->first();



        if($repair == null){
            return 1;
        }

        return $repair->order + 1;

    }
}
