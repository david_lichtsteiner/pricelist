<?php

namespace App\Http\Controllers;

use App\Device;
use App\DeviceGroup;
use Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;

class PricelistController extends Controller
{
    public function index(){
        $deviceGroups= DeviceGroup::all();
        return view('deviceCategory.list',compact('deviceGroups'));
    }

    public function store(){
        $input = Request::all();
        DeviceGroup::create($input);


        return redirect('pricelist');
    }

    public function destroy(DeviceGroup $deviceGroup){

        $deviceGroup->delete();
        return redirect('pricelist');
    }

    public function update(DeviceGroup $deviceGroup){
        $input = Request::all();
        $deviceGroup->update($input);
        return redirect('pricelist');
    }


}
