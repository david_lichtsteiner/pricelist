<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class ChangePasswordController extends Controller
{


    public function showChangeForm(Request $request, $token = null)
    {
        return view('auth.passwords.change')->with(compact('token', 'email'));
    }


    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function changePassword($user, $password)
    {
        $user->password = bcrypt($password);

        $user->save();

    }



    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        $credentials = $request->only(
            'old_password', 'password'
        );

        if (!Hash::check($credentials['old_password'], Auth::user()->password)) {
            return $this->sendFailedUpdateResponse();
        }

        $this->changePassword(Auth::user(),$credentials['password']);

        return redirect('/');
    }

    protected function sendFailedUpdateResponse()
    {
        return redirect()->back()
            ->withErrors([
                $this->oldPassword() => $this->getFailedUpdateMessage(),
            ]);
    }

    protected function getFailedUpdateMessage()
    {
        return Lang::has('auth.failedUpdate')
            ? Lang::get('auth.failedUpdate')
            : 'Das eingegebene Passwort ist nicht korrekt';
    }

    public function oldPassword()
    {
        return property_exists($this, 'password') ? $this->username : 'old_password';
    }
}
