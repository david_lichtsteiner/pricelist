<?php

namespace App\Http\Controllers;

use App\OpeningTime;
use Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Region;
use App\Branch;


/**
 * Class BranchController
 * @package App\Http\Controllers
 */
class BranchController extends Controller
{
    public function index(){
       $branches = Branch::orderBy('order','asc')->get();
       return view('branch.list',compact('branches'));
    }

    public function create(){
        $regions = $this->prepareRegionsForForm();
        return view('branch.create',compact('regions'));
    }

    public function store(Requests\CreatBranchRequest $request){

        $input = Request::all();
        $input["order"] = $this->getOrder($input["region_id"]);
        //dd($input);
        $input = $this->getAllCheckboxValues($input);
        $branch = Branch::create($input);

        for($i=1;$i<=7;$i++){
            $newOpeningTime = ["branch_id"=>$branch->id,"day"=>$i,"opens"=>$input[$i.'_opens'],"closes"=>$input[$i.'_closes'],'is_open'=>true];
            OpeningTime::create($newOpeningTime);
        }

        return redirect('filialen');
    }

    public function destroy(Branch $branch){
        $branch->delete();

        return redirect('filialen');
    }

    public function edit(Branch $branch){
        $regions = $this->prepareRegionsForForm();
        $opening_times = $branch->openingTimes()->orderBy('day','asc')->get()->toArray();

        foreach($opening_times as $day)
        {
            $branch[$day['day'].'_opens'] = date('H:i',strtotime($day["opens"]));

            $branch[$day['day'].'_closes'] = date('H:i',strtotime($day["closes"]));
        }

        return view('branch.edit',compact('regions','branch','opening_times'));
    }




    public function update(Branch $branch, Requests\CreatBranchRequest $request)
    {
        $input = Request::all();
        $input = $this->getAllCheckboxValues($input);

        $branch->update($input);


        $opening_times = $branch->openingTimes()->orderBy('day','asc')->get();

        for($i=1;$i<=7;$i++){
            $updatedOpeningTime = ["branch_id"=>$branch['id'],"day"=>$i,"opens"=>$input[$i.'_opens'],"closes"=>$input[$i.'_closes'],'is_open'=>$input[$i.'_is_open']];
            $day =  $opening_times[$i-1];
            $opening_times[$i-1]->update($updatedOpeningTime);
        }

        return redirect('filialen');

    }


    /**
     *
     * For all checkbox values in the the form get true or false.
     *
     * @param $entity
     * @return mixed
     */
    private function getAllCheckboxValues($entity)
    {
        $entity = $this->getCheckboxValue($entity, 'show_online');
        $entity = $this->getCheckboxValue($entity, 'show_info');
        $entity = $this->getCheckboxValue($entity, '1_is_open');
        $entity = $this->getCheckboxValue($entity, '2_is_open');
        $entity = $this->getCheckboxValue($entity, '3_is_open');
        $entity = $this->getCheckboxValue($entity, '4_is_open');
        $entity = $this->getCheckboxValue($entity, '5_is_open');
        $entity = $this->getCheckboxValue($entity, '6_is_open');
        $entity = $this->getCheckboxValue($entity, '7_is_open');

        return $entity;
    }

    /**
     *
     * Get true or false from a check box value
     *
     * @param $entity
     * @param $key
     * @return mixed
     */
    private function getCheckboxValue($entity, $key) {
        if(array_key_exists($key, $entity)){
            $entity[$key] = true;
        } else {
            $entity[$key] = false;
        }
        return $entity;
    }


    /**
     *
     * Generate a named array for use in the view with a select element.
     *
     * @return array
     */
    public function prepareRegionsForForm(){
        $regions_entities = Region::select(['id','name'])->get()->toArray();
        $regions = array();

        foreach($regions_entities as $regions_entity){
            $regions[$regions_entity['id']] =  $regions_entity['name'];
        }

        return $regions;
    }


    private function getOrder($regionID){


        $branch = Branch::where('region_id','=',$regionID)->orderBy('order','desc')->get()->first();
        if($branch == null){

            return Region::where('id','=',$regionID)->get()->first()->order;
        }

        return $branch->order + 1;

    }

}
