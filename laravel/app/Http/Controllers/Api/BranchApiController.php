<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Branch;
use Illuminate\Support\Facades\Mail;

class BranchApiController extends Controller
{

     public function index(Branch $branch)
    {
        $branch["regionLowerCase"] = strtolower($branch["region"]["name"]);

        $location = json_decode(file_get_contents('https://www.treestones.org/decenturi/branch_workaround.php?placeid='.$branch["place_ID"]));
     /*   $location = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?placeid=' . $branch["place_ID"] . '&key=AIzaSyBh5Mgc88E9mAG_iLDcEZfLu9pm7SHhbuQ')); */

        if (property_exists($location,'result'))
        {
            if (property_exists($location->result, 'rating'))
            {
                $branch['rating'] = $location->result->rating;
                $branch['google_url'] = $location->result->url;
            } else
            {
                $rating = null;
            }
        }else
        {
            $this->sendErrorEmail();
        }



        $branch['ratingRoundedToHalf'] = round(($branch['rating']*2), 0)/2;


        $branch["formattedPhone"] = $this->formatphonenumber($branch);




        $openingTimesText = $this->getOpeningTimeText($branch);




        return view('api.branch',compact('branch','openingTimesText'));
    }
    public function phonelist(){
        $branches = Branch::all();
        foreach ($branches as $branch){
            $branch["formattedPhone"] = $this->formatphonenumber($branch);
            $branch["phoneLinkNumber"] = $this->phoneLink($branch);
        }
        return view('api.phonenumbers', compact('branches'));
    }

    public function detailInfo(Branch $branch){
        $branch["formattedPhone"] = $this->formatphonenumber($branch);
        $branch["phoneLinkNumber"] = $this->phoneLink($branch);
        $openingTimesText = $this->getOpeningTimeText($branch);
        return view('api.detailbranch', compact('openingTimesText','branch'));
    }

    public function formatphonenumber(Branch $branch){
        $formattedPhone = "0". $branch->phone;
        $formattedPhone = substr($formattedPhone,0,3).' '.substr($formattedPhone,3,3).' '.substr($formattedPhone,6,2)
            .' '.substr($formattedPhone,8,2);
        return $formattedPhone;
    }

    public function phoneLink(Branch $branch){
        $phoneLinkNumber = "+41". $branch->phone;
        return $phoneLinkNumber;
    }

    public function getOpeningTimeText($branch){
        $openingTimes = $branch->openingTimes()->orderBy('day','asc')->get();
        $openingTimesText = "";
        $countEqualDaysInRow = 0;
        $daysShortNameLookup = ["Mo","Di","Mi","Do","Fr","Sa","So"];

        for($day = 1; $day < 7; $day++){
            if($openingTimes[$day-1]->compareTimes($openingTimes[$day]))
            {
                $countEqualDaysInRow++;
            } else {
                if ($countEqualDaysInRow > 0)
                {
                    $openingTimesText .=$daysShortNameLookup[$day - ($countEqualDaysInRow+1)]. " - "
                        . $daysShortNameLookup[$day - 1] . ": ";
                    $openingTimesText .= date('H:i', strtotime($openingTimes[$day-1]->opens))." - ".
                        date('H:i', strtotime($openingTimes[$day-1]->closes));
                    $openingTimesText .= "<br />";
                    $countEqualDaysInRow = 0;
                }

                $openingTime = $openingTimes[$day];
                if ($openingTime->is_open)
                {
                    $openingTimesText .= $daysShortNameLookup[$day] .": ";
                    $openingTimesText .= date('H:i', strtotime($openingTime->opens)). ' - '. date('H:i', strtotime($openingTime->closes));
                    $openingTimesText .= "<br />";
                }
            }
        }

        return  $openingTimesText;
    }


    public function sendErrorEmail(){
        $messageText = "Limit of API Calls for decenturi.ch has been reached";


    /*    Mail::send('mail.error', ['text' => $messageText], function ($m)  {


            $m->from('manage@decenturi.ch', 'Manage Decenturi');

            $m->to('info@treestones.ch', 'Tree Stones')->subject('Error with Google API');
        });*/
    }
}
