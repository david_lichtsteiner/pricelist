<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\DeviceGroup;
use App\Device;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PricelistApiController extends Controller
{
    public function index() {
        $deviceGroups = DeviceGroup::orderBy('order','asc')->get();

        foreach($deviceGroups as $deviceGroup){
            $cssID = strtolower($deviceGroup['name']);
            $cssID = str_replace(' ', '-', $cssID);
            $deviceGroup['cssID'] = $cssID;
        }

        return view('api.pricelist',compact('deviceGroups'));
    }

    public function indexDeviceGroup(DeviceGroup $deviceGroup){
        $deviceTitle = $deviceGroup->name;
        $devices = $deviceGroup->devices()->getEager();
        foreach($devices as $device){
            $device->cssID = $this->getCSSIdForDeviceGroup($device);
        }
        return view('api.pricelistdevicegroup',compact('devices','deviceTitle'));
    }

    public function getCSSIdForDeviceGroup(Device $device)
    {
        return  strtolower(str_replace([' ','/'],'',$device->name));
    }
    
    public function indexDevices(Device $device){
        $deviceName = $device->name;
        return view('api.pricelistdevice',compact('device','deviceName'));
    }
}


