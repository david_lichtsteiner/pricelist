<?php

namespace App\Http\Controllers;


use Request;
use App\Http\Requests;
use \App\Repair;
use App\Http\Controllers\Controller;

class RepairController extends Controller
{

    public function update(Repair $repair){
        //dd($repair);
        $field = Request::get('name');
        //dd($field);
        // dd(Request::get('value'));
        $repair->$field = Request::get('value');
        $repair->save();
    }

    public function store(Requests\RepairRequest $request){
        $input = Request::all();
        $input["order"] = $this->getOrder( $input["device_id"]);


        Repair::create($input);

        return redirect('/device/'.Request::get('device_id'));

    }

    public function destroy(Repair $repair){
       $device_id = $repair->device_id;

       $repair->delete();

        return redirect('device/'.$device_id);
    }

    public function updateOrder(){
        $order = Request::all();
        $array = $order["repair"];
        $size = count($array);
        for($i = 0; $i < $size; $i++){
            $id = $array[$i];
            $newOrder = $i;
            Repair::find($id)->update(['order'=> $newOrder]);
        }
    }

    private function getOrder($deviceID){


        $repair = Repair::where('device_id','=',$deviceID)->orderBy('order','desc')->get()->first();



        if($repair == null){
            return 1;
        }

        return $repair->order + 1;

    }

}
