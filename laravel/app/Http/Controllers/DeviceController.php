<?php

namespace App\Http\Controllers;

use Request;
use App\Device;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DeviceController extends Controller
{
    public function show(Device $device){

        $repairs = $device->repairs()->orderBy('order','asc')->get();
        $services = $device->services()->orderBy('order','asc')->get();

        return view('device.repairs', compact('repairs','device','services'));

    }

    public function store(Device $device)
    {

        $input = Request::all();
        $input["order"] = $this->getOrder($input["device_group_id"]);
        $input["show_online"] = 1;

        $branch = Device::create($input);
        return redirect('device/'.$branch->id);
    }

    public function destroy(Device $device){

        $device->delete();
        return redirect('pricelist');
    }

    public function update(Device $device)
    {
        $field = Request::get('name');
        $device->$field = Request::get('value');
        $device->save();
    }

    public function updateOrder(){
        $order = Request::all();
        $keys = array_keys($order);
        $array = $order[$keys[0]];
        $size = count($array);
        for($i = 0; $i < $size; $i++){
            $id = $array[$i];
            $newOrder = $size - $i;
            Device::find($id)->update(['order'=> $newOrder]);
        }

    }



    public function getOrder($deviceGroup){
        $branch = Device::where('device_group_id','=',$deviceGroup)->orderBy('order','desc')->get()->first();

        if($branch == null){
            return 1;
        }

        return $branch->order + 1;

    }

}
