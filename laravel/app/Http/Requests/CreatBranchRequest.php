<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreatBranchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'street_1'=>'required',
            'post_code'=>'required|integer|between:1000,9999',
            'city'  => 'required|string|min:2',
            'phone' => 'required|integer|between:100000000,999999999',


            '1_opens'=>'date_format:"H:i"|required_unless:1_is_open,'.false.'',
            '1_closes'=>'date_format:"H:i"|required_unless:1_is_open,'.false.'',
            '2_opens'=>'date_format:"H:i"|required_unless:2_is_open,'.false.'',
            '2_closes'=>'date_format:"H:i"|required_unless:2_is_open,'.false.'',
            '3_opens'=>'date_format:"H:i"|required_unless:3_is_open,'.false.'',
            '3_closes'=>'date_format:"H:i"|required_unless:3_is_open,'.false.'',
            '4_opens'=>'date_format:"H:i"|required_unless:4_is_open,'.false.'',
            '4_closes'=>'date_format:"H:i"|required_unless:4_is_open,'.false.'',
            '5_opens'=>'date_format:"H:i"|required_unless:5_is_open,'.false.'',
            '5_closes'=>'date_format:"H:i"|required_unless:5_is_open,'.false.'',
            '6_opens'=>'date_format:"H:i"|required_unless:6_is_open,'.false.'',
            '6_closes'=>'date_format:"H:i"|required_unless:6_is_open,'.false.'',
            '7_opens'=>'date_format:"H:i"|required_unless:7_is_open,'.false.'',
            '7_closes'=>'date_format:"H:i"|required_unless:7_is_open,'.false.'',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Bitte geben Sie einen Namen ein.',
            'street_1.required'=>'Bitte geben Sie einen Adresse ein.',

            'post_code.required' => 'Bitte geben Sie eine Postleitzahl ein',
            'post_code.integer' => 'Bitte geben Sie eine gültige Postleitzahl ein',
            'post_code.between' => 'Bitte geben Sie eine gültige Postleitzahl ein',

            'city.required' => 'Bitte geben Sie einen Ort ein',


            'phone.required' => "Bitte geben Sie eine Telefonnummer ein",
            'phone.integer' => "Bitte geben Sie eine gültige Telefonnummer ein",
            'phone.between' => "Bitte geben Sie eine gültige Telefonnummer ein",

            '1_opens.date:format' => 'Bitte geben Sie die Zeit im Format h:m an z.B: 09:00',
            '1_opens.required_unless' => 'Bitte geben Sie eine Öffungs-Zeit für Montag ein',
            '1_closes.required_unless' => 'Bitte geben Sie eine Schliessungs-Zeit für Montag ein',

            '2_opens.date:format' => 'Bitte geben Sie die Zeit im Format h:m an z.B: 09:00',
            '2_opens.required_unless' => 'Bitte geben Sie eine Öffungs-Zeit für Dienstag ein',
            '2_closes.required_unless' => 'Bitte geben Sie eine Schliessungs-Zeit für Dienstag ein',

            '3_opens.date:format' => 'Bitte geben Sie die Zeit im Format h:m an z.B: 09:00',
            '3_opens.required_unless' => 'Bitte geben Sie eine Öffungs-Zeit für Mittwoch ein',
            '3_closes.required_unless' => 'Bitte geben Sie eine Schliessungs-Zeit für Mittwoch ein',


            '4_opens.date:format' => 'Bitte geben Sie die Zeit im Format h:m an z.B: 09:00',
            '4_opens.required_unless' => 'Bitte geben Sie eine Öffungs-Zeit für Donnerstag ein',
            '4_closes.required_unless' => 'Bitte geben Sie eine Schliessungs-Zeit für Donnerstag ein',

            '5_opens.date:format' => 'Bitte geben Sie die Zeit im Format h:m an z.B: 09:00',
            '5_opens.required_unless' => 'Bitte geben Sie eine Öffungs-Zeit für Freitag ein',
            '5_closes.required_unless' => 'Bitte geben Sie eine Schliessungs-Zeit für Freitag ein',

            '6_opens.date:format' => 'Bitte geben Sie die Zeit im Format h:m an z.B: 09:00',
            '6_opens.required_unless' => 'Bitte geben Sie eine Öffungs-Zeit für Samstag ein',
            '6_closes.required_unless' => 'Bitte geben Sie eine Schliessungs-Zeit für Samstag ein',

            '7_opens.date:format' => 'Bitte geben Sie die Zeit im Format h:m an z.B: 09:00',
            '7_opens.required_unless' => 'Bitte geben Sie eine Öffungs-Zeit für Sonntag ein',
            '7_closes.required_unless' => 'Bitte geben Sie eine Schliessungs-Zeit für Sonntag ein',
        ];
    }

}
