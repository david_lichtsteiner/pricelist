<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => 'web'], function () {

    Route::get('api/html/pricelist','PricelistApiController@index');
    Route::get('api/html/pricelist/{devicegroup}','PricelistApiController@indexDeviceGroup');
    Route::get('api/html/device/{device}','PricelistApiController@indexDevices');
    Route::get('api/html/branch/detail/{branch}','BranchApiController@detailInfo');
    Route::get('api/html/branch/phonenumber','BranchApiController@phonelist');
    Route::get('api/html/branch/{branch}','BranchApiController@index');
    
});