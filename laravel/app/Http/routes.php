<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => 'web'], function () {
    Route::get('/', 'Auth\AuthController@showLoginForm');
    Route::auth();
    Route::get('/home', 'HomeController@index');


    Route::group(['middleware' => 'auth'], function ()
    {
        // Password Change Routes
        Route::get('password/change', 'Auth\ChangePasswordController@showChangeForm');
        Route::post('password/update', 'Auth\ChangePasswordController@update');


        Route::resource('filialen','BranchController');

        Route::get('pricelist','PricelistController@index');
        Route::post('pricelist','PricelistController@store');
        Route::delete('pricelist/{pricelist}','PricelistController@destroy');
        Route::patch('pricelist/{pricelist}','PricelistController@update');


        Route::post('device','DeviceController@store');
        Route::post('device/order','DeviceController@updateOrder');
        Route::post('device/edit/{device}','DeviceController@update');
        Route::get('device/{device}','DeviceController@show');
        Route::delete('device/{device}','DeviceController@destroy');


        Route::post('repair/edit/{repair}','RepairController@update');
        Route::post('repair/order','RepairController@updateOrder');
        Route::post('repair','RepairController@store');
        Route::delete('repair/{repair}','RepairController@destroy');

        Route::post('service/edit/{service}','ServiceController@update');
        Route::post('service/order','ServiceController@updateOrder');
        Route::post('service','ServiceController@store');
        Route::delete('service/{service}','ServiceController@destroy');
    });

});