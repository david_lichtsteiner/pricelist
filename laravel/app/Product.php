<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'device_id','description', 'price_chf', 'is_service', 'order'
    ];

    public function device()
    {
        return $this->belongsTo('App\Device');
    }
}
