<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceGroup extends Model
{
    protected $fillable = [
      'name', 'order'
    ];

    public function devices(){
        return $this->hasMany('App\Device');
    }
}
