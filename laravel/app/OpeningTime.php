<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpeningTime extends Model
{
    protected $fillable = [
        'day','branch_id','opens','closes','is_open'
    ];

    public function compareTimes($openingTime){
        if($this->opens == $openingTime->opens && $this->closes == $openingTime->closes ){
            return true;
        }
        else {
            return false;
        }
    }
}


