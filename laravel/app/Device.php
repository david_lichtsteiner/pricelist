<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = [
      'name', 'device_group_id', 'show_online', 'order'
    ];

    public function repairs(){
        return $this->hasMany('App\Repair');
    }

    public function services(){
        return $this->hasMany('App\Service');
    }
    
}
