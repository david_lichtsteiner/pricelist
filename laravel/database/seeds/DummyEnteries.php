<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DummyEnteries extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'name'=>'David',
            'email'=>'david.lichtsteiner@treestones.ch',
            'password'=>bcrypt('1234')
        ]);

        DB::table('users')->insert([
            'name'=>'Test',
            'email'=>'test',
            'password'=>bcrypt('1234')
        ]);


        DB::table('device_groups')->insert([
            'name'=> 'iPhone',
            'order'=>100
        ]);

        DB::table('device_groups')->insert([
            'name'=> 'Samsung Galaxy',
            'order'=>200
        ]);

        DB::table('device_groups')->insert([
            'name'=> 'iPad',
            'order'=>500
        ]);

        DB::table('device_groups')->insert([
            'name'=> 'HTC',
            'order'=>300
        ]);

        DB::table('device_groups')->insert([
            'name'=> 'Sony Xperia',
            'order'=>400
        ]);

    }
}
