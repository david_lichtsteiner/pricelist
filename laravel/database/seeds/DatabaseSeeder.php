<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RegionTableSeeder::class);
        $this->call(DummyBranchSeeder::class);
        $this->call(OpeningTimes::class);
        $this->call(DummyEnteries::class);
        $this->call(Devices::class);
        $this->call(RepairSeeder::class);
        $this->call(ServiceSeeder::class);
    }
}
