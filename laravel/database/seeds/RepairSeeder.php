<?php

use Illuminate\Database\Seeder;

class RepairSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('repairs')->insert( [
            'id'=>89,
            'device_id'=>16,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:07:41',
            'updated_at'=>'2016-02-24 09:07:41'
        ] );



        DB::table('repairs')->insert( [
            'id'=>83,
            'device_id'=>17,
            'description'=>'SIM Tray Austausch (SIM-Karten Halter)',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>7,
            'created_at'=>'2016-02-24 10:06:36',
            'updated_at'=>'2016-02-24 09:06:36'
        ] );



        DB::table('repairs')->insert( [
            'id'=>78,
            'device_id'=>17,
            'description'=>'Rückseite Cover Austausch',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:06:58',
            'updated_at'=>'2016-02-24 09:06:58'
        ] );



        DB::table('repairs')->insert( [
            'id'=>77,
            'device_id'=>17,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:07:02',
            'updated_at'=>'2016-02-24 09:07:02'
        ] );



        DB::table('repairs')->insert( [
            'id'=>79,
            'device_id'=>17,
            'description'=>'Akku Austausch',
            'price_chf'=>'79.00',
            'is_service'=>0,
            'order'=>3,
            'created_at'=>'2016-02-24 10:06:53',
            'updated_at'=>'2016-02-24 09:06:53'
        ] );



        DB::table('repairs')->insert( [
            'id'=>80,
            'device_id'=>17,
            'description'=>'Home Button Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>4,
            'created_at'=>'2016-02-24 10:06:51',
            'updated_at'=>'2016-02-24 09:06:51'
        ] );



        DB::table('repairs')->insert( [
            'id'=>81,
            'device_id'=>17,
            'description'=>'USB Ladebuchse Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>5,
            'created_at'=>'2016-02-24 10:06:47',
            'updated_at'=>'2016-02-24 09:06:47'
        ] );



        DB::table('repairs')->insert( [
            'id'=>82,
            'device_id'=>17,
            'description'=>'Power, Volume, Mute Button Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>6,
            'created_at'=>'2016-02-24 10:06:42',
            'updated_at'=>'2016-02-24 09:06:42'
        ] );



        DB::table('repairs')->insert( [
            'id'=>88,
            'device_id'=>17,
            'description'=>'Antenne Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>12,
            'created_at'=>'2016-02-24 10:06:04',
            'updated_at'=>'2016-02-24 09:06:04'
        ] );



        DB::table('repairs')->insert( [
            'id'=>84,
            'device_id'=>17,
            'description'=>'Vibra Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>8,
            'created_at'=>'2016-02-24 10:06:28',
            'updated_at'=>'2016-02-24 09:06:28'
        ] );



        DB::table('repairs')->insert( [
            'id'=>85,
            'device_id'=>17,
            'description'=>'Frontseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>9,
            'created_at'=>'2016-02-24 10:06:22',
            'updated_at'=>'2016-02-24 09:06:22'
        ] );



        DB::table('repairs')->insert( [
            'id'=>86,
            'device_id'=>17,
            'description'=>'Rückseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>10,
            'created_at'=>'2016-02-24 10:06:15',
            'updated_at'=>'2016-02-24 09:06:15'
        ] );



        DB::table('repairs')->insert( [
            'id'=>87,
            'device_id'=>17,
            'description'=>'Lautsprecher Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>11,
            'created_at'=>'2016-02-24 10:06:09',
            'updated_at'=>'2016-02-24 09:06:09'
        ] );



        DB::table('repairs')->insert( [
            'id'=>70,
            'device_id'=>18,
            'description'=>'Power, Volume, Mute Button Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>5,
            'created_at'=>'2016-02-24 10:05:12',
            'updated_at'=>'2016-02-24 09:05:12'
        ] );



        DB::table('repairs')->insert( [
            'id'=>68,
            'device_id'=>18,
            'description'=>'Home Button Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>3,
            'created_at'=>'2016-02-24 10:05:21',
            'updated_at'=>'2016-02-24 09:05:21'
        ] );



        DB::table('repairs')->insert( [
            'id'=>67,
            'device_id'=>18,
            'description'=>'Akku Austausch',
            'price_chf'=>'79.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:05:23',
            'updated_at'=>'2016-02-24 09:05:23'
        ] );



        DB::table('repairs')->insert( [
            'id'=>76,
            'device_id'=>18,
            'description'=>'Antenne Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>11,
            'created_at'=>'2016-02-24 10:04:49',
            'updated_at'=>'2016-02-24 09:04:49'
        ] );



        DB::table('repairs')->insert( [
            'id'=>71,
            'device_id'=>18,
            'description'=>'SIM Tray Austausch (SIM-Karten Halter)',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>6,
            'created_at'=>'2016-02-24 10:05:07',
            'updated_at'=>'2016-02-24 09:05:07'
        ] );



        DB::table('repairs')->insert( [
            'id'=>72,
            'device_id'=>18,
            'description'=>'Vibra Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>7,
            'created_at'=>'2016-02-24 10:05:03',
            'updated_at'=>'2016-02-24 09:05:03'
        ] );



        DB::table('repairs')->insert( [
            'id'=>69,
            'device_id'=>18,
            'description'=>'USB Ladebuchse Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>4,
            'created_at'=>'2016-02-24 10:05:33',
            'updated_at'=>'2016-02-24 09:05:33'
        ] );



        DB::table('repairs')->insert( [
            'id'=>66,
            'device_id'=>18,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'149.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:05:26',
            'updated_at'=>'2016-02-24 09:05:26'
        ] );



        DB::table('repairs')->insert( [
            'id'=>75,
            'device_id'=>18,
            'description'=>'Lautsprecher Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>10,
            'created_at'=>'2016-02-24 10:04:52',
            'updated_at'=>'2016-02-24 09:04:52'
        ] );



        DB::table('repairs')->insert( [
            'id'=>74,
            'device_id'=>18,
            'description'=>'Rückseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>9,
            'created_at'=>'2016-02-24 10:04:57',
            'updated_at'=>'2016-02-24 09:04:57'
        ] );



        DB::table('repairs')->insert( [
            'id'=>73,
            'device_id'=>18,
            'description'=>'Frontseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>8,
            'created_at'=>'2016-02-24 10:05:01',
            'updated_at'=>'2016-02-24 09:05:01'
        ] );



        DB::table('repairs')->insert( [
            'id'=>56,
            'device_id'=>19,
            'description'=>'Akku Austausch',
            'price_chf'=>'79.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:03:35',
            'updated_at'=>'2016-02-24 09:03:35'
        ] );



        DB::table('repairs')->insert( [
            'id'=>57,
            'device_id'=>19,
            'description'=>'USB Ladebuchse Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>3,
            'created_at'=>'2016-02-24 10:03:39',
            'updated_at'=>'2016-02-24 09:03:39'
        ] );



        DB::table('repairs')->insert( [
            'id'=>58,
            'device_id'=>19,
            'description'=>'Power, Volume, Mute Button Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>4,
            'created_at'=>'2016-02-24 10:03:44',
            'updated_at'=>'2016-02-24 09:03:44'
        ] );



        DB::table('repairs')->insert( [
            'id'=>59,
            'device_id'=>19,
            'description'=>'Antenne Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>5,
            'created_at'=>'2016-02-24 10:03:47',
            'updated_at'=>'2016-02-24 09:03:47'
        ] );



        DB::table('repairs')->insert( [
            'id'=>60,
            'device_id'=>19,
            'description'=>'Home Button Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>6,
            'created_at'=>'2016-02-24 10:03:51',
            'updated_at'=>'2016-02-24 09:03:51'
        ] );



        DB::table('repairs')->insert( [
            'id'=>61,
            'device_id'=>19,
            'description'=>'SIM Tray Austausch (SIM-Karten Halter)',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>7,
            'created_at'=>'2016-02-24 10:04:04',
            'updated_at'=>'2016-02-24 09:04:04'
        ] );



        DB::table('repairs')->insert( [
            'id'=>63,
            'device_id'=>19,
            'description'=>'Frontseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>9,
            'created_at'=>'2016-02-24 10:04:13',
            'updated_at'=>'2016-02-24 09:04:13'
        ] );



        DB::table('repairs')->insert( [
            'id'=>64,
            'device_id'=>19,
            'description'=>'Rückseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>10,
            'created_at'=>'2016-02-24 10:04:16',
            'updated_at'=>'2016-02-24 09:04:16'
        ] );



        DB::table('repairs')->insert( [
            'id'=>65,
            'device_id'=>19,
            'description'=>'Lautsprecher Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>11,
            'created_at'=>'2016-02-24 10:04:21',
            'updated_at'=>'2016-02-24 09:04:21'
        ] );



        DB::table('repairs')->insert( [
            'id'=>62,
            'device_id'=>19,
            'description'=>'Vibra Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>8,
            'created_at'=>'2016-02-24 10:04:09',
            'updated_at'=>'2016-02-24 09:04:09'
        ] );



        DB::table('repairs')->insert( [
            'id'=>55,
            'device_id'=>19,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'149.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:03:33',
            'updated_at'=>'2016-02-24 09:03:33'
        ] );



        DB::table('repairs')->insert( [
            'id'=>53,
            'device_id'=>20,
            'description'=>'SIM Tray Austausch (SIM-Karten Halter)	',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>10,
            'created_at'=>'2016-02-24 07:39:47',
            'updated_at'=>'2016-02-24 07:39:47'
        ] );



        DB::table('repairs')->insert( [
            'id'=>54,
            'device_id'=>20,
            'description'=>'Vibra Austausch	',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>11,
            'created_at'=>'2016-02-24 07:39:54',
            'updated_at'=>'2016-02-24 07:39:54'
        ] );



        DB::table('repairs')->insert( [
            'id'=>44,
            'device_id'=>20,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'149.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 08:38:14',
            'updated_at'=>'2016-02-24 07:38:14'
        ] );



        DB::table('repairs')->insert( [
            'id'=>45,
            'device_id'=>20,
            'description'=>'Akku Austausch',
            'price_chf'=>'79.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 07:38:31',
            'updated_at'=>'2016-02-24 07:38:31'
        ] );



        DB::table('repairs')->insert( [
            'id'=>46,
            'device_id'=>20,
            'description'=>'USB Ladebuchse Austausch	',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>3,
            'created_at'=>'2016-02-24 07:38:39',
            'updated_at'=>'2016-02-24 07:38:39'
        ] );



        DB::table('repairs')->insert( [
            'id'=>47,
            'device_id'=>20,
            'description'=>'Power, Volume, Mute Button Austausch	',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>4,
            'created_at'=>'2016-02-24 07:38:48',
            'updated_at'=>'2016-02-24 07:38:48'
        ] );



        DB::table('repairs')->insert( [
            'id'=>48,
            'device_id'=>20,
            'description'=>'Antenne Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>5,
            'created_at'=>'2016-02-24 07:38:58',
            'updated_at'=>'2016-02-24 07:38:58'
        ] );



        DB::table('repairs')->insert( [
            'id'=>49,
            'device_id'=>20,
            'description'=>'Rückseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>6,
            'created_at'=>'2016-02-24 07:39:11',
            'updated_at'=>'2016-02-24 07:39:11'
        ] );



        DB::table('repairs')->insert( [
            'id'=>50,
            'device_id'=>20,
            'description'=>'Frontseite Kamera Austausch	',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>7,
            'created_at'=>'2016-02-24 07:39:18',
            'updated_at'=>'2016-02-24 07:39:18'
        ] );



        DB::table('repairs')->insert( [
            'id'=>51,
            'device_id'=>20,
            'description'=>'Home Button Austausch	',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>8,
            'created_at'=>'2016-02-24 07:39:27',
            'updated_at'=>'2016-02-24 07:39:27'
        ] );



        DB::table('repairs')->insert( [
            'id'=>52,
            'device_id'=>20,
            'description'=>'Lautsprecher Austausch	',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>9,
            'created_at'=>'2016-02-24 07:39:35',
            'updated_at'=>'2016-02-24 07:39:35'
        ] );



        DB::table('repairs')->insert( [
            'id'=>36,
            'device_id'=>21,
            'description'=>'Antenne Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>8,
            'created_at'=>'2016-02-24 10:02:23',
            'updated_at'=>'2016-02-24 09:02:23'
        ] );



        DB::table('repairs')->insert( [
            'id'=>37,
            'device_id'=>21,
            'description'=>'SIM Tray Austausch (SIM-Karten Halter)',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>7,
            'created_at'=>'2016-02-24 10:02:20',
            'updated_at'=>'2016-02-24 09:02:20'
        ] );



        DB::table('repairs')->insert( [
            'id'=>42,
            'device_id'=>21,
            'description'=>'Frontseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 06:02:30',
            'updated_at'=>'2016-02-24 06:02:30'
        ] );



        DB::table('repairs')->insert( [
            'id'=>38,
            'device_id'=>21,
            'description'=>'USB Ladebuchse Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>6,
            'created_at'=>'2016-02-24 10:02:16',
            'updated_at'=>'2016-02-24 09:02:16'
        ] );



        DB::table('repairs')->insert( [
            'id'=>39,
            'device_id'=>21,
            'description'=>'Rückseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>5,
            'created_at'=>'2016-02-24 10:02:08',
            'updated_at'=>'2016-02-24 09:02:08'
        ] );



        DB::table('repairs')->insert( [
            'id'=>41,
            'device_id'=>21,
            'description'=>'Lautsprecher Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>3,
            'created_at'=>'2016-02-24 10:01:58',
            'updated_at'=>'2016-02-24 09:01:58'
        ] );



        DB::table('repairs')->insert( [
            'id'=>40,
            'device_id'=>21,
            'description'=>'Akku Austausch',
            'price_chf'=>'79.00',
            'is_service'=>0,
            'order'=>4,
            'created_at'=>'2016-02-24 10:02:03',
            'updated_at'=>'2016-02-24 09:02:03'
        ] );



        DB::table('repairs')->insert( [
            'id'=>43,
            'device_id'=>21,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'179.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 06:02:40',
            'updated_at'=>'2016-02-24 06:02:40'
        ] );



        DB::table('repairs')->insert( [
            'id'=>35,
            'device_id'=>22,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'219.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:01:02',
            'updated_at'=>'2016-02-24 09:01:02'
        ] );



        DB::table('repairs')->insert( [
            'id'=>34,
            'device_id'=>22,
            'description'=>'Frontseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:01:08',
            'updated_at'=>'2016-02-24 09:01:08'
        ] );



        DB::table('repairs')->insert( [
            'id'=>33,
            'device_id'=>22,
            'description'=>'Lautsprecher Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>3,
            'created_at'=>'2016-02-24 10:01:13',
            'updated_at'=>'2016-02-24 09:01:13'
        ] );



        DB::table('repairs')->insert( [
            'id'=>32,
            'device_id'=>22,
            'description'=>'Plus Akku Austausch',
            'price_chf'=>'79.00',
            'is_service'=>0,
            'order'=>4,
            'created_at'=>'2016-02-24 06:00:17',
            'updated_at'=>'2016-02-24 06:00:17'
        ] );



        DB::table('repairs')->insert( [
            'id'=>31,
            'device_id'=>22,
            'description'=>'USB Ladebuchse',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>5,
            'created_at'=>'2016-02-24 10:01:21',
            'updated_at'=>'2016-02-24 09:01:21'
        ] );



        DB::table('repairs')->insert( [
            'id'=>30,
            'device_id'=>23,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'299.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 09:56:02',
            'updated_at'=>'2016-02-24 08:56:02'
        ] );



        DB::table('repairs')->insert( [
            'id'=>23,
            'device_id'=>23,
            'description'=>' Antenne Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>8,
            'created_at'=>'2016-02-24 09:56:33',
            'updated_at'=>'2016-02-24 08:56:33'
        ] );



        DB::table('repairs')->insert( [
            'id'=>24,
            'device_id'=>23,
            'description'=>'SIM Tray Austausch (SIM-Karten Halter)',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>7,
            'created_at'=>'2016-02-24 09:56:30',
            'updated_at'=>'2016-02-24 08:56:30'
        ] );



        DB::table('repairs')->insert( [
            'id'=>25,
            'device_id'=>23,
            'description'=>'USB Ladebuchse Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>6,
            'created_at'=>'2016-02-24 09:56:22',
            'updated_at'=>'2016-02-24 08:56:22'
        ] );



        DB::table('repairs')->insert( [
            'id'=>26,
            'device_id'=>23,
            'description'=>'Rückseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>5,
            'created_at'=>'2016-02-24 09:56:18',
            'updated_at'=>'2016-02-24 08:56:18'
        ] );



        DB::table('repairs')->insert( [
            'id'=>27,
            'device_id'=>23,
            'description'=>'Akku Austausch',
            'price_chf'=>'79.00',
            'is_service'=>0,
            'order'=>4,
            'created_at'=>'2016-02-24 09:56:12',
            'updated_at'=>'2016-02-24 08:56:12'
        ] );



        DB::table('repairs')->insert( [
            'id'=>28,
            'device_id'=>23,
            'description'=>'Lautsprecher Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>3,
            'created_at'=>'2016-02-24 09:56:09',
            'updated_at'=>'2016-02-24 08:56:09'
        ] );



        DB::table('repairs')->insert( [
            'id'=>29,
            'device_id'=>23,
            'description'=>'Frontseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 09:56:05',
            'updated_at'=>'2016-02-24 08:56:05'
        ] );



        DB::table('repairs')->insert( [
            'id'=>17,
            'device_id'=>24,
            'description'=>'Akku Austausch',
            'price_chf'=>'79.00',
            'is_service'=>0,
            'order'=>4,
            'created_at'=>'2016-02-24 09:55:11',
            'updated_at'=>'2016-02-24 08:55:11'
        ] );



        DB::table('repairs')->insert( [
            'id'=>18,
            'device_id'=>24,
            'description'=>'Lautsprecher Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>3,
            'created_at'=>'2016-02-24 09:55:04',
            'updated_at'=>'2016-02-24 08:55:04'
        ] );



        DB::table('repairs')->insert( [
            'id'=>19,
            'device_id'=>24,
            'description'=>'Frontseite Kamera Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 09:54:58',
            'updated_at'=>'2016-02-24 08:54:58'
        ] );



        DB::table('repairs')->insert( [
            'id'=>22,
            'device_id'=>24,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'399.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 09:54:53',
            'updated_at'=>'2016-02-24 08:54:53'
        ] );



        DB::table('repairs')->insert( [
            'id'=>16,
            'device_id'=>24,
            'description'=>'USB Ladebuchse',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>5,
            'created_at'=>'2016-02-24 09:55:16',
            'updated_at'=>'2016-02-24 08:55:16'
        ] );



        DB::table('repairs')->insert( [
            'id'=>115,
            'device_id'=>27,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'149.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:13:20',
            'updated_at'=>'2016-02-24 09:13:20'
        ] );



        DB::table('repairs')->insert( [
            'id'=>114,
            'device_id'=>28,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'179.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:13:07',
            'updated_at'=>'2016-02-24 09:13:07'
        ] );



        DB::table('repairs')->insert( [
            'id'=>113,
            'device_id'=>29,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'229.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:12:52',
            'updated_at'=>'2016-02-24 09:12:52'
        ] );



        DB::table('repairs')->insert( [
            'id'=>111,
            'device_id'=>30,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'189.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:12:37',
            'updated_at'=>'2016-02-24 09:12:37'
        ] );



        DB::table('repairs')->insert( [
            'id'=>110,
            'device_id'=>31,
            'description'=>'Rückseite Cover Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:11:57',
            'updated_at'=>'2016-02-24 09:11:57'
        ] );



        DB::table('repairs')->insert( [
            'id'=>109,
            'device_id'=>31,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'189.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:12:00',
            'updated_at'=>'2016-02-24 09:12:00'
        ] );



        DB::table('repairs')->insert( [
            'id'=>107,
            'device_id'=>32,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'189.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:11:31',
            'updated_at'=>'2016-02-24 09:11:31'
        ] );



        DB::table('repairs')->insert( [
            'id'=>108,
            'device_id'=>32,
            'description'=>'Rückseite Cover Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:11:37',
            'updated_at'=>'2016-02-24 09:11:37'
        ] );



        DB::table('repairs')->insert( [
            'id'=>106,
            'device_id'=>33,
            'description'=>'Edge Plus Rückseite Cover Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:11:14',
            'updated_at'=>'2016-02-24 09:11:14'
        ] );



        DB::table('repairs')->insert( [
            'id'=>105,
            'device_id'=>33,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'299.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:11:04',
            'updated_at'=>'2016-02-24 09:11:04'
        ] );



        DB::table('repairs')->insert( [
            'id'=>104,
            'device_id'=>34,
            'description'=>'Rückseite Cover Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:10:55',
            'updated_at'=>'2016-02-24 09:10:55'
        ] );



        DB::table('repairs')->insert( [
            'id'=>103,
            'device_id'=>34,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'249.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:10:48',
            'updated_at'=>'2016-02-24 09:10:48'
        ] );



        DB::table('repairs')->insert( [
            'id'=>101,
            'device_id'=>35,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'199.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:10:34',
            'updated_at'=>'2016-02-24 09:10:34'
        ] );



        DB::table('repairs')->insert( [
            'id'=>102,
            'device_id'=>35,
            'description'=>'Rückseite Cover Austausch',
            'price_chf'=>'99.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:10:39',
            'updated_at'=>'2016-02-24 09:10:39'
        ] );



        DB::table('repairs')->insert( [
            'id'=>100,
            'device_id'=>36,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'189.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:10:00',
            'updated_at'=>'2016-02-24 09:10:00'
        ] );



        DB::table('repairs')->insert( [
            'id'=>99,
            'device_id'=>37,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'189.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:09:43',
            'updated_at'=>'2016-02-24 09:09:43'
        ] );



        DB::table('repairs')->insert( [
            'id'=>97,
            'device_id'=>38,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'149.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:09:33',
            'updated_at'=>'2016-02-24 09:09:33'
        ] );



        DB::table('repairs')->insert( [
            'id'=>95,
            'device_id'=>39,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'179.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:09:18',
            'updated_at'=>'2016-02-24 09:09:18'
        ] );



        DB::table('repairs')->insert( [
            'id'=>96,
            'device_id'=>39,
            'description'=>'Rückseite Cover Austausch',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:09:21',
            'updated_at'=>'2016-02-24 09:09:21'
        ] );



        DB::table('repairs')->insert( [
            'id'=>94,
            'device_id'=>40,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'149.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:09:08',
            'updated_at'=>'2016-02-24 09:09:08'
        ] );



        DB::table('repairs')->insert( [
            'id'=>93,
            'device_id'=>41,
            'description'=>'Rückseite Cover Austausch',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:08:49',
            'updated_at'=>'2016-02-24 09:08:49'
        ] );



        DB::table('repairs')->insert( [
            'id'=>92,
            'device_id'=>41,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'179.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:45',
            'updated_at'=>'2016-02-24 09:08:45'
        ] );



        DB::table('repairs')->insert( [
            'id'=>91,
            'device_id'=>42,
            'description'=>' Cover Austausch',
            'price_chf'=>'39.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:08:35',
            'updated_at'=>'2016-02-24 09:08:35'
        ] );



        DB::table('repairs')->insert( [
            'id'=>90,
            'device_id'=>42,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'69.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>116,
            'device_id'=>43,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'239.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>117,
            'device_id'=>43,
            'description'=>'Glas Austausch: Glas & Touchscreen',
            'price_chf'=>'199.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>118,
            'device_id'=>44,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'269.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>119,
            'device_id'=>44,
            'description'=>'Glas Austausch: Glas & Touchscreen',
            'price_chf'=>'249.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>120,
            'device_id'=>45,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'199.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>121,
            'device_id'=>45,
            'description'=>'Glas Austausch: Glas & Touchscreen',
            'price_chf'=>'169.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>122,
            'device_id'=>46,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'229.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>123,
            'device_id'=>46,
            'description'=>'Glas Austausch: Glas & Touchscreen',
            'price_chf'=>'189.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>124,
            'device_id'=>47,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'229.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>125,
            'device_id'=>47,
            'description'=>'Glas Austausch: Glas & Touchscreen',
            'price_chf'=>'189.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>126,
            'device_id'=>48,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'229.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>127,
            'device_id'=>48,
            'description'=>'Glas Austausch: Glas & Touchscreen',
            'price_chf'=>'189.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>128,
            'device_id'=>49,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'229.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>129,
            'device_id'=>49,
            'description'=>'Glas Austausch: Glas & Touchscreen',
            'price_chf'=>'189.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>130,
            'device_id'=>50,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'249.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>131,
            'device_id'=>51,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'219.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>132,
            'device_id'=>52,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'249.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );


        DB::table('repairs')->insert( [
            'id'=>133,
            'device_id'=>53,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'249.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>134,
            'device_id'=>54,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'249.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );

        DB::table('repairs')->insert( [
            'id'=>135,
            'device_id'=>55,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'219.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );


        DB::table('repairs')->insert( [
            'id'=>136,
            'device_id'=>56,
            'description'=>'Frontseite komplett Austausch: LCD Display Glas Touchscreen',
            'price_chf'=>'199.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:08:32',
            'updated_at'=>'2016-02-24 09:08:32'
        ] );






    }
}
