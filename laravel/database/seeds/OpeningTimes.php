<?php

use Illuminate\Database\Seeder;

class OpeningTimes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('opening_Times')->insert( [
            'id'=>1,
            'branch_id'=>1,
            'day'=>1,
            'opens'=>'10:00:00',
            'closes'=>'18:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:17:48',
            'updated_at'=>'2016-02-22 16:17:48'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>2,
            'branch_id'=>1,
            'day'=>2,
            'opens'=>'10:00:00',
            'closes'=>'18:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:17:48',
            'updated_at'=>'2016-02-22 16:17:48'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>3,
            'branch_id'=>1,
            'day'=>3,
            'opens'=>'10:00:00',
            'closes'=>'18:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:17:48',
            'updated_at'=>'2016-02-22 16:17:48'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>4,
            'branch_id'=>1,
            'day'=>4,
            'opens'=>'10:00:00',
            'closes'=>'21:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:17:48',
            'updated_at'=>'2016-02-22 16:17:48'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>5,
            'branch_id'=>1,
            'day'=>5,
            'opens'=>'10:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:17:48',
            'updated_at'=>'2016-02-22 16:17:48'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>6,
            'branch_id'=>1,
            'day'=>6,
            'opens'=>'09:00:00',
            'closes'=>'16:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:17:48',
            'updated_at'=>'2016-02-22 16:17:48'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>7,
            'branch_id'=>1,
            'day'=>7,
            'opens'=>'00:00:00',
            'closes'=>'00:00:00',
            'is_open'=>0,
            'created_at'=>'2016-02-22 17:17:48',
            'updated_at'=>'2016-02-22 16:17:48'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>8,
            'branch_id'=>2,
            'day'=>1,
            'opens'=>'10:00:00',
            'closes'=>'18:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:16:02',
            'updated_at'=>'2016-02-22 16:16:02'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>9,
            'branch_id'=>2,
            'day'=>2,
            'opens'=>'10:00:00',
            'closes'=>'18:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:16:02',
            'updated_at'=>'2016-02-22 16:16:02'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>10,
            'branch_id'=>2,
            'day'=>3,
            'opens'=>'10:00:00',
            'closes'=>'18:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:16:02',
            'updated_at'=>'2016-02-22 16:16:02'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>11,
            'branch_id'=>2,
            'day'=>4,
            'opens'=>'10:00:00',
            'closes'=>'21:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:16:02',
            'updated_at'=>'2016-02-22 16:16:02'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>12,
            'branch_id'=>2,
            'day'=>5,
            'opens'=>'10:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:16:02',
            'updated_at'=>'2016-02-22 16:16:02'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>13,
            'branch_id'=>2,
            'day'=>6,
            'opens'=>'09:00:00',
            'closes'=>'16:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 17:16:02',
            'updated_at'=>'2016-02-22 16:16:02'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>14,
            'branch_id'=>2,
            'day'=>7,
            'opens'=>'00:00:00',
            'closes'=>'00:00:00',
            'is_open'=>0,
            'created_at'=>'2016-02-22 17:16:02',
            'updated_at'=>'2016-02-22 16:16:02'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>15,
            'branch_id'=>3,
            'day'=>1,
            'opens'=>'10:00:00',
            'closes'=>'18:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:19:58',
            'updated_at'=>'2016-02-22 16:19:58'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>16,
            'branch_id'=>3,
            'day'=>2,
            'opens'=>'10:00:00',
            'closes'=>'18:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:19:58',
            'updated_at'=>'2016-02-22 16:19:58'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>17,
            'branch_id'=>3,
            'day'=>3,
            'opens'=>'10:00:00',
            'closes'=>'18:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:19:58',
            'updated_at'=>'2016-02-22 16:19:58'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>18,
            'branch_id'=>3,
            'day'=>4,
            'opens'=>'10:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:19:58',
            'updated_at'=>'2016-02-22 16:19:58'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>19,
            'branch_id'=>3,
            'day'=>5,
            'opens'=>'10:00:00',
            'closes'=>'18:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:19:58',
            'updated_at'=>'2016-02-22 16:19:58'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>20,
            'branch_id'=>3,
            'day'=>6,
            'opens'=>'10:00:00',
            'closes'=>'18:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:19:58',
            'updated_at'=>'2016-02-22 16:19:58'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>21,
            'branch_id'=>3,
            'day'=>7,
            'opens'=>'00:00:00',
            'closes'=>'00:00:00',
            'is_open'=>0,
            'created_at'=>'2016-02-22 16:19:58',
            'updated_at'=>'2016-02-22 16:19:58'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>22,
            'branch_id'=>4,
            'day'=>1,
            'opens'=>'09:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>23,
            'branch_id'=>4,
            'day'=>2,
            'opens'=>'09:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>24,
            'branch_id'=>4,
            'day'=>3,
            'opens'=>'09:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );



        DB::table('opening_Times')->insert( [
            'id'=>25,
            'branch_id'=>4,
            'day'=>4,
            'opens'=>'09:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>26,
            'branch_id'=>4,
            'day'=>5,
            'opens'=>'09:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>27,
            'branch_id'=>4,
            'day'=>6,
            'opens'=>'09:00:00',
            'closes'=>'18:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>28,
            'branch_id'=>4,
            'day'=>7,
            'opens'=>'00:00:00',
            'closes'=>'00:00:00',
            'is_open'=>0,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>29,
            'branch_id'=>5,
            'day'=>1,
            'opens'=>'08:30:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>30,
            'branch_id'=>5,
            'day'=>2,
            'opens'=>'08:30:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>31,
            'branch_id'=>5,
            'day'=>3,
            'opens'=>'08:30:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>32,
            'branch_id'=>5,
            'day'=>4,
            'opens'=>'08:30:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>33,
            'branch_id'=>5,
            'day'=>5,
            'opens'=>'08:30:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>34,
            'branch_id'=>5,
            'day'=>6,
            'opens'=>'08:00:00',
            'closes'=>'18:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>35,
            'branch_id'=>5,
            'day'=>7,
            'opens'=>'00:00:00',
            'closes'=>'00:00:00',
            'is_open'=>0,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>36,
            'branch_id'=>6,
            'day'=>1,
            'opens'=>'09:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>37,
            'branch_id'=>6,
            'day'=>2,
            'opens'=>'09:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>38,
            'branch_id'=>6,
            'day'=>3,
            'opens'=>'09:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>39,
            'branch_id'=>6,
            'day'=>4,
            'opens'=>'09:00:00',
            'closes'=>'20:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>40,
            'branch_id'=>6,
            'day'=>5,
            'opens'=>'09:00:00',
            'closes'=>'21:30:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>41,
            'branch_id'=>6,
            'day'=>6,
            'opens'=>'08:00:00',
            'closes'=>'17:00:00',
            'is_open'=>1,
            'created_at'=>'2016-02-22 16:28:17',
            'updated_at'=>'2016-02-22 16:28:17'
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>42,
            'branch_id'=>6,
            'day'=>7,
            'opens'=>'00:00:00',
            'closes'=>'00:00:00',
            'is_open'=>0,
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>43,
            'branch_id'=>7,
            'day'=>1,
            'opens'=>'10:00:00',
            'closes'=>'19:00:00',
            'is_open'=>1,
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>44,
            'branch_id'=>7,
            'day'=>2,
            'opens'=>'10:00:00',
            'closes'=>'19:00:00',
            'is_open'=>1,
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>45,
            'branch_id'=>7,
            'day'=>3,
            'opens'=>'10:00:00',
            'closes'=>'19:00:00',
            'is_open'=>1,
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>46,
            'branch_id'=>7,
            'day'=>4,
            'opens'=>'10:00:00',
            'closes'=>'19:00:00',
            'is_open'=>1,
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>47,
            'branch_id'=>7,
            'day'=>5,
            'opens'=>'10:00:00',
            'closes'=>'19:00:00',
            'is_open'=>1,
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>48,
            'branch_id'=>7,
            'day'=>6,
            'opens'=>'10:00:00',
            'closes'=>'18:00:00',
            'is_open'=>1,
        ] );

        DB::table('opening_Times')->insert( [
            'id'=>49,
            'branch_id'=>7,
            'day'=>7,
            'opens'=>'00:00:00',
            'closes'=>'00:00:00',
            'is_open'=>0,
        ] );


    }
}
