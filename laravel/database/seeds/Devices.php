<?php

use Illuminate\Database\Seeder;

class Devices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('devices')->insert( [
            'id'=>16,
            'device_group_id'=>1,
            'name'=>' iPhone 3G / iPhone 3GS',
            'show_online'=>1,
            'order'=>1,
            'created_at'=>'2016-02-23 10:46:55',
            'updated_at'=>'2016-02-23 10:46:55'
        ] );

        DB::table('devices')->insert( [
            'id'=>17,
            'device_group_id'=>1,
            'name'=>'iPhone 4 / iPhone 4S',
            'show_online'=>1,
            'order'=>2,
            'created_at'=>'2016-02-23 10:47:09',
            'updated_at'=>'2016-02-23 10:47:09'
        ] );

        DB::table('devices')->insert( [
            'id'=>18,
            'device_group_id'=>1,
            'name'=>'iPhone 5',
            'show_online'=>1,
            'order'=>3,
            'created_at'=>'2016-02-23 11:50:30',
            'updated_at'=>'2016-02-23 10:47:18'
        ] );

        DB::table('devices')->insert( [
            'id'=>19,
            'device_group_id'=>1,
            'name'=>'iPhone 5S ',
            'show_online'=>1,
            'order'=>4,
            'created_at'=>'2016-02-23 10:47:26',
            'updated_at'=>'2016-02-23 10:47:26'
        ] );

        DB::table('devices')->insert( [
            'id'=>20,
            'device_group_id'=>1,
            'name'=>'iPhone 5C',
            'show_online'=>1,
            'order'=>5,
            'created_at'=>'2016-02-23 10:47:37',
            'updated_at'=>'2016-02-23 10:47:37'
        ] );

        DB::table('devices')->insert( [
            'id'=>21,
            'device_group_id'=>1,
            'name'=>'iPhone 6',
            'show_online'=>1,
            'order'=>6,
            'created_at'=>'2016-02-23 10:47:47',
            'updated_at'=>'2016-02-23 10:47:47'
        ] );

        DB::table('devices')->insert( [
            'id'=>22,
            'device_group_id'=>1,
            'name'=>'iPhone 6 Plus ',
            'show_online'=>1,
            'order'=>7,
            'created_at'=>'2016-02-23 10:48:04',
            'updated_at'=>'2016-02-23 10:48:04'
        ] );

        DB::table('devices')->insert( [
            'id'=>23,
            'device_group_id'=>1,
            'name'=>'iPhone 6S ',
            'show_online'=>1,
            'order'=>8,
            'created_at'=>'2016-02-23 10:48:11',
            'updated_at'=>'2016-02-23 10:48:11'
        ] );

        DB::table('devices')->insert( [
            'id'=>24,
            'device_group_id'=>1,
            'name'=>'iPhone 6S Plus',
            'show_online'=>1,
            'order'=>9,
            'created_at'=>'2016-02-23 10:48:20',
            'updated_at'=>'2016-02-23 10:48:20'
        ] );

        /**************************************************************************************************************/

        DB::table('devices')->insert( [
            'id'=>32,
            'device_group_id'=>2,
            'name'=>'Galaxy Alpha',
            'show_online'=>1,
            'order'=>1,
            'created_at'=>'2016-02-23 10:56:58',
            'updated_at'=>'2016-02-23 10:56:58'
        ] );

        DB::table('devices')->insert( [
            'id'=>27,
            'device_group_id'=>2,
            'name'=>'Galaxy A3 A300',
            'show_online'=>1,
            'order'=>2,
            'created_at'=>'2016-02-23 10:55:54',
            'updated_at'=>'2016-02-23 10:55:54'
        ] );


        DB::table('devices')->insert( [
            'id'=>31,
            'device_group_id'=>2,
            'name'=>'Galaxy A5',
            'show_online'=>1,
            'order'=>3,
            'created_at'=>'2016-02-23 10:56:47',
            'updated_at'=>'2016-02-23 10:56:47'
        ] );


        DB::table('devices')->insert( [
            'id'=>30,
            'device_group_id'=>2,
            'name'=>'Galaxy Note 3',
            'show_online'=>1,
            'order'=>4,
            'created_at'=>'2016-02-23 10:56:34',
            'updated_at'=>'2016-02-23 10:56:34'
        ] );

        DB::table('devices')->insert( [
            'id'=>29,
            'device_group_id'=>2,
            'name'=>'Galaxy Note 4',
            'show_online'=>1,
            'order'=>5,
            'created_at'=>'2016-02-23 10:56:22',
            'updated_at'=>'2016-02-23 10:56:22'
        ] );

        DB::table('devices')->insert( [
            'id'=>42,
            'device_group_id'=>2,
            'name'=>'Galaxy S2',
            'show_online'=>1,
            'order'=>6,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>40,
            'device_group_id'=>2,
            'name'=>'Galaxy S3 Mini',
            'show_online'=>1,
            'order'=>7,
            'created_at'=>'2016-02-23 10:58:58',
            'updated_at'=>'2016-02-23 10:58:58'
        ] );

        DB::table('devices')->insert( [
            'id'=>41,
            'device_group_id'=>2,
            'name'=>'Galaxy S3',
            'show_online'=>1,
            'order'=>8,
            'created_at'=>'2016-02-23 10:59:07',
            'updated_at'=>'2016-02-23 10:59:07'
        ] );

        DB::table('devices')->insert( [
            'id'=>38,
            'device_group_id'=>2,
            'name'=>'Galaxy S4 Mini',
            'show_online'=>1,
            'order'=>9,
            'created_at'=>'2016-02-23 10:58:32',
            'updated_at'=>'2016-02-23 10:58:32'
        ] );

        DB::table('devices')->insert( [
            'id'=>39,
            'device_group_id'=>2,
            'name'=>'Galaxy S4',
            'show_online'=>1,
            'order'=>10,
            'created_at'=>'2016-02-23 10:58:43',
            'updated_at'=>'2016-02-23 10:58:43'
        ] );

        DB::table('devices')->insert( [
            'id'=>28,
            'device_group_id'=>2,
            'name'=>'Galaxy S5 Neo',
            'show_online'=>1,
            'order'=>11,
            'created_at'=>'2016-02-23 10:56:09',
            'updated_at'=>'2016-02-23 10:56:09'
        ] );

        DB::table('devices')->insert( [
            'id'=>36,
            'device_group_id'=>2,
            'name'=>'Galaxy S5 Mini',
            'show_online'=>1,
            'order'=>12,
            'created_at'=>'2016-02-23 11:57:57',
            'updated_at'=>'2016-02-23 10:57:41'
        ] );

        DB::table('devices')->insert( [
            'id'=>37,
            'device_group_id'=>2,
            'name'=>'Galaxy S5 ',
            'show_online'=>1,
            'order'=>13,
            'created_at'=>'2016-02-23 10:58:13',
            'updated_at'=>'2016-02-23 10:58:13'
        ] );

        DB::table('devices')->insert( [
            'id'=>34,
            'device_group_id'=>2,
            'name'=>'Galaxy S6 Edge',
            'show_online'=>1,
            'order'=>14,
            'created_at'=>'2016-02-23 10:57:19',
            'updated_at'=>'2016-02-23 10:57:19'
        ] );

        DB::table('devices')->insert( [
            'id'=>33,
            'device_group_id'=>2,
            'name'=>'Galaxy S6 Edge Plus',
            'show_online'=>1,
            'order'=>15,
            'created_at'=>'2016-02-23 10:57:09',
            'updated_at'=>'2016-02-23 10:57:09'
        ] );

        DB::table('devices')->insert( [
            'id'=>35,
            'device_group_id'=>2,
            'name'=>'Galaxy S6',
            'show_online'=>1,
            'order'=>16,
            'created_at'=>'2016-02-23 10:57:30',
            'updated_at'=>'2016-02-23 10:57:30'
        ] );








        /**************************************************************************************************************/

        DB::table('devices')->insert( [
            'id'=>43,
            'device_group_id'=>3,
            'name'=>'iPad Air 1 ',
            'show_online'=>1,
            'order'=>6,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>44,
            'device_group_id'=>3,
            'name'=>'iPad Air 2',
            'show_online'=>1,
            'order'=>7,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>45,
            'device_group_id'=>3,
            'name'=>'iPad 2',
            'show_online'=>1,
            'order'=>3,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>46,
            'device_group_id'=>3,
            'name'=>'iPad 3',
            'show_online'=>1,
            'order'=>4,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>47,
            'device_group_id'=>3,
            'name'=>'iPad 4',
            'show_online'=>1,
            'order'=>5,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>48,
            'device_group_id'=>3,
            'name'=>'iPad Mini 2',
            'show_online'=>1,
            'order'=>2,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>49,
            'device_group_id'=>3,
            'name'=>'iPad Mini',
            'show_online'=>1,
            'order'=>1,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>50,
            'device_group_id'=>5,
            'name'=>'Sony Z3',
            'show_online'=>1,
            'order'=>2,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>51,
            'device_group_id'=>5,
            'name'=>'Sony Z3 Compact',
            'show_online'=>1,
            'order'=>1,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>52,
            'device_group_id'=>5,
            'name'=>'Sony Z5',
            'show_online'=>1,
            'order'=>4,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>53,
            'device_group_id'=>5,
            'name'=>'Sony Z5 Compact',
            'show_online'=>1,
            'order'=>3,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );


        DB::table('devices')->insert( [
            'id'=>54,
            'device_group_id'=>4,
            'name'=>'HTC One A9 ',
            'show_online'=>1,
            'order'=>3,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>55,
            'device_group_id'=>4,
            'name'=>'HTC One M9 ',
            'show_online'=>1,
            'order'=>2,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );

        DB::table('devices')->insert( [
            'id'=>56,
            'device_group_id'=>4,
            'name'=>'HTC One M8 ',
            'show_online'=>1,
            'order'=>1,
            'created_at'=>'2016-02-23 10:59:17',
            'updated_at'=>'2016-02-23 10:59:17'
        ] );



    }
}
