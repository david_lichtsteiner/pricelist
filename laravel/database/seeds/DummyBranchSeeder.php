<?php

use Illuminate\Database\Seeder;

class DummyBranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert([
            'name' => 'Luzern Rössligasse',
            'region_id'=>10,
            'street_1'=>"Rössligasse 17",
            'street_2' => 'vis a vis Coop City',
            'post_code'=>6004,
            'city'=>'Luzern',
            'phone'=>'412660333',
            'place_ID' => 'ChIJPT7msZ_7j0cR9mvGWOmHr3Q',
            'order'=>100,
        ]);

        DB::table('branches')->insert( [
            'id'=>2,
            'region_id'=>10,
            'name'=>'Luzern Kapellgasse',
            'street_1'=>'Kapellgasse 26',
            'street_2'=>'',
            'post_code'=>6004,
            'city'=>'Luzern',
            'phone'=>'412660571',
            'place_ID'=>'ChIJS9lr4p77j0cRzNDOZkrJT2c',
            'order'=>101,
        ] );

        DB::table('branches')->insert( [
            'id'=>3,
            'region_id'=>3,
            'name'=>'Basel Gerbergasse',
            'street_1'=>'Gerbergasse 70 ',
            'street_2'=>'',
            'post_code'=>4001,
            'city'=>'Basel',
            'phone'=>'612610785',
            'place_ID' => 'ChIJDeJrpK25kUcRcvy3VvqYeJ8',
            'order'=>400,
        ] );



        DB::table('branches')->insert( [
            'id'=>4,
            'region_id'=>3,
            'name'=>' Basel St. Jakobs-Strasse',
            'street_1'=>'St. Jakobs-Strasse 397',
            'street_2'=>'EKZ St. Jakob-Park ',
            'post_code'=>4052,
            'city'=>'Basel',
            'phone'=>'613116020',
            'place_ID' => 'ChIJRdBteii4kUcRVKG5JKoEZRQ',
            'order'=>401,
        ] );



        DB::table('branches')->insert( [
            'id'=>5,
            'region_id'=>3,
            'name'=>'Basel Greifengasse',
            'street_1'=>'Greifengasse 19 ',
            'street_2'=>'',
            'post_code'=>4058,
            'city'=>'Basel',
            'phone'=>'612222310',
            'place_ID' => 'ChIJ2c30_K-5kUcRmFniVZEu-N4',
            'order'=>402,
        ] );

        DB::table('branches')->insert( [
            'id'=>6,
            'region_id'=>4,
            'name'=>'Bern',
            'street_1'=>'Industriestrasse 10',
            'street_2'=>'Shoppyland Schönbühl',
            'post_code'=>3321,
            'city'=>'Schönbühl',
            'phone'=>'318520901',
            'place_ID' => 'ChIJvU68a3owjkcRCFE_EnbNShM',
            'order'=>500,
        ] );

        DB::table('branches')->insert( [
            'id'=>7,
            'region_id'=>24,
            'name'=>'Zürich Mühlegasse',
            'street_1'=>'Mühlegasse 7',
            'street_2'=>'',
            'post_code'=>8001,
            'city'=>'Zürich',
            'phone'=>'432437254',
            'place_ID' => 'ChIJkZftMKigmkcRWldwKyi_wzQ',
            'order'=>2400,
        ] );

    }
}
