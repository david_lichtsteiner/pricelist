<?php

use Illuminate\Database\Seeder;
use App\Repair;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        DB::table('services')->insert( [
            'id'=>2,
            'device_id'=>24,
            'description'=>'Datenübertragung / Backup',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 09:55:34',
            'updated_at'=>'2016-02-24 08:55:34'
        ] );

        DB::table('services')->insert( [
            'id'=>3,
            'device_id'=>24,
            'description'=>'Softwareaktualisierung',
            'price_chf'=>'19.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 09:55:45',
            'updated_at'=>'2016-02-24 08:55:45'
        ] );

        DB::table('services')->insert( [
            'id'=>4,
            'device_id'=>23,
            'description'=>'Datenübertragung / Backup',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 09:56:59',
            'updated_at'=>'2016-02-24 08:56:59'
        ] );

        DB::table('services')->insert( [
            'id'=>5,
            'device_id'=>23,
            'description'=>'Softwareaktualisierung',
            'price_chf'=>'19.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 09:57:03',
            'updated_at'=>'2016-02-24 08:57:03'
        ] );

        DB::table('services')->insert( [
            'id'=>6,
            'device_id'=>22,
            'description'=>'iPhone 6 Plus Dienstleistung (Datenübertragung / Backup)',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 08:50:45',
            'updated_at'=>'2016-02-24 08:50:45'
        ] );

        DB::table('services')->insert( [
            'id'=>7,
            'device_id'=>22,
            'description'=>'Softwareaktualisierung',
            'price_chf'=>'19.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:01:35',
            'updated_at'=>'2016-02-24 09:01:35'
        ] );

        DB::table('services')->insert( [
            'id'=>8,
            'device_id'=>21,
            'description'=>'Datenübertragung / Backup',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:02:33',
            'updated_at'=>'2016-02-24 09:02:33'
        ] );

        DB::table('services')->insert( [
            'id'=>9,
            'device_id'=>21,
            'description'=>'Softwareaktualisierung',
            'price_chf'=>'19.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:02:38',
            'updated_at'=>'2016-02-24 09:02:38'
        ] );

        DB::table('services')->insert( [
            'id'=>10,
            'device_id'=>20,
            'description'=>'Datenübertragung / Backup',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:02:56',
            'updated_at'=>'2016-02-24 09:02:56'
        ] );

        DB::table('services')->insert( [
            'id'=>11,
            'device_id'=>20,
            'description'=>'Softwareaktualisierung',
            'price_chf'=>'19.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:03:00',
            'updated_at'=>'2016-02-24 09:03:00'
        ] );

        DB::table('services')->insert( [
            'id'=>12,
            'device_id'=>19,
            'description'=>'Datenübertragung / Backup',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:03:21',
            'updated_at'=>'2016-02-24 09:03:21'
        ] );

        DB::table('services')->insert( [
            'id'=>13,
            'device_id'=>19,
            'description'=>'Softwareaktualisierung',
            'price_chf'=>'19.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:03:26',
            'updated_at'=>'2016-02-24 09:03:26'
        ] );

        DB::table('services')->insert( [
            'id'=>14,
            'device_id'=>18,
            'description'=>'Datenübertragung / Backup',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:04:39',
            'updated_at'=>'2016-02-24 09:04:39'
        ] );

        DB::table('services')->insert( [
            'id'=>15,
            'device_id'=>18,
            'description'=>'Softwareaktualisierung',
            'price_chf'=>'19.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:04:44',
            'updated_at'=>'2016-02-24 09:04:44'
        ] );

        DB::table('services')->insert( [
            'id'=>16,
            'device_id'=>17,
            'description'=>'Datenübertragung / Backup',
            'price_chf'=>'49.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:05:55',
            'updated_at'=>'2016-02-24 09:05:55'
        ] );

        DB::table('services')->insert( [
            'id'=>17,
            'device_id'=>17,
            'description'=>'Softwareaktualisierung',
            'price_chf'=>'19.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:05:59',
            'updated_at'=>'2016-02-24 09:05:59'
        ] );

        DB::table('services')->insert( [
            'id'=>18,
            'device_id'=>16,
            'description'=>'Datenübertragung / Backup',
            'price_chf'=>'19.00',
            'is_service'=>0,
            'order'=>1,
            'created_at'=>'2016-02-24 10:07:52',
            'updated_at'=>'2016-02-24 09:07:52'
        ] );

        DB::table('services')->insert( [
            'id'=>19,
            'device_id'=>16,
            'description'=>'Softwareaktualisierung',
            'price_chf'=>'19.00',
            'is_service'=>0,
            'order'=>2,
            'created_at'=>'2016-02-24 10:08:02',
            'updated_at'=>'2016-02-24 09:08:02'
        ] );

    }
}
