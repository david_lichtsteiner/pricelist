<?php

use Illuminate\Database\Seeder;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = ['Aargau', 'Appenzell', 'Basel', 'Bern', 'Freiburg', 'Genf', 'Glarus', 'Graubünden', 'Jura',
            'Luzern','Neuenburg','Nidwalden','Obwalden','Schaffhausen','Schwyz','Solothurn','St. Gallen',
            'Tessin', 'Thurgau', 'Uri', 'Waadt', 'Wallis', 'Zug', 'Zürich'];

        $i = 200;
        foreach ($regions as $region){
            if($region == "Luzern"){
                $order = 100;
            }else{
                $order = $i;
                $i = $i + 100;
            }


            DB::table('regions')->insert([
                'name' => $region,
                'country' => 'SUI',
                'order' => $order
            ]);
        }
    }


}
