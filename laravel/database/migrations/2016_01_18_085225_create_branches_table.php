<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('region_id')->unsigned();
            $table->string('name');
            $table->string('street_1');
            $table->string('street_2')->nullable();
            $table->smallInteger('post_code')->unsigned();
            $table->string('city');
            $table->string('phone');
            $table->text('info')->nullable();
            $table->boolean('show_info');
            $table->integer('order');
            $table->string('place_ID');
            $table->timestamps();

            $table->foreign('region_id')
                ->references('id')
                ->on('regions');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branches');
    }
}
