<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('device_group_id')->unsigned();
            $table->text('name');
            $table->boolean('show_online');
            $table->integer('order');
            $table->timestamps();

            $table->foreign('device_group_id')
                ->references('id')
                ->on('device_groups');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('devices');
    }
}
