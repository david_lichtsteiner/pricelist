@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <a href="/filialen">
            <div class="col-md-4 col-md-offset-2">
                <div class="panel panel-default main-group location">
                    <div class="panel-body">
                        Filialen verwalten
                    </div>
                </div>
            </div>
        </a>

        <a href="/pricelist">
            <div class="col-md-4">
                <div class="panel panel-default main-group pricelist">
                    <div class="panel-body">
                        Preisliste
                    </div>
                </div>
            </div>
        </a>

    </div>
</div>
@endsection
