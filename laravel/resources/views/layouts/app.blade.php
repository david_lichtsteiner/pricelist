<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf" content="{!! csrf_token() !!}">

    <title>Decentool</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <script src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>

    <!-- Styles -->
    <link href="{!!  asset('assets/bootstrap/css/bootstrap.min.css')  !!}" rel="stylesheet">
    <link href="{!!  asset('assets/css/bootstrap-switch.css')  !!}" rel="stylesheet">
    <link href="{!!  asset('assets/css/bootstrap-switch.css')  !!}"  rel="stylesheet"/>
    <link href="{!!  elixir('css/app.css')  !!}"  rel="stylesheet"/>

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">

                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (!Auth::guest())
                       <li>
                           <a href="/filialen" class="dropdown-toggle"  role="button" aria-expanded="false">
                              Filialen
                           </a>
                       </li>
                        <li>
                            <a href="/pricelist" class="dropdown-toggle"  role="button" aria-expanded="false">
                                Preisliste
                            </a>
                        </li>
                        @if(Auth::user()->isAdmin())
                            <li>
                                <a href="/register" class="dropdown-toggle"  role="button" aria-expanded="false">
                                    Benutzer
                                </a>
                            </li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-key"></i>Passwort</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>

                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <link href="{!!  asset('assets/editable/css/bootstrap-editable.css')  !!}" rel="stylesheet">
    <script src="{!!  asset('assets/editable/js/bootstrap-editable.js')  !!}"></script>

    <script src="{!!  asset('assets/js/bootstrap-switch.js')  !!}"></script>
   <!-- <script src="{!! asset('assets/js/app.js') !!}"></script> -->

    @yield('javascript')


    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
