@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>
            <a data-pk="{!! $device->id !!}" href="#" data-name="name" data-type="text"
               data-title="Neue Reparatur" data-url="{!! URL::to('/device/edit/'.$device->id) !!}"
               name="description" class="edit">
                {!! $device->name !!}</a>
          </h1>
        <a class="btn btn-primary pull-right back-button" href="/pricelist">Zurück</a>
        <hr />

        <div class="row">
            <div class="col-md-6">
                <h3>Reparaturen</h3>
                <a href="#addRepairModal" data-toggle="modal" data-link="/repair" data-title="Neue Reparatur hinzufügen" class="action add-repair">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true" ></span>
                </a>
                <hr/>
                <table id="repairs" class="repair_table table table-striped" width="50%">
                    <thead>
                    <tr>
                        <th>Reperatur</th>
                        <th width="10%">Preis</th>
                        <th width="10%">Löschen</th>

                    </tr>
                    </thead>

                    @foreach($repairs as $repair)
                        <tr id="repair_{!! $repair->id !!}">
                            <td ><a data-pk="{!! $device->id !!}" href="#" data-name="description" data-type="text"
                                    data-title="Neue Reperatur" data-url="{!! URL::to('/repair/edit/'.$repair->id) !!}"
                                    name="description" class="edit">
                                    {!! $repair->description !!}</a></td>
                            <td width="10%"><a data-pk="{!! $device->id !!}" href="#" data-name="price_chf" data-type="text"
                                                data-title="Beschreibung" data-url="{!! URL::to('/repair/edit/'.$repair->id) !!}"
                                                name="price_chf" class="edit">
                                {!! $repair->price_chf !!}</a></td>
                            <td width="10%">
                                <a href={!! URL::to('/repair/'.$repair->id) !!} data-method="delete"
                                   data-token="{{csrf_token()}}" data-confirm="Sind sie sicher, dass die Sie Reperatur {!! $repair->description !!} löschen möchten">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true" ></span>
                                </a>
                            </td>
                        </tr>



                    @endforeach

                </table>
            </div>
            <div class="col-md-6">
                <h3>Dienstleistungen </h3>
                <a href="#addRepairModal" data-toggle="modal" data-link="/service" data-title="Neue Dienstleistung hinzufügen" class="action add-repair">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true" ></span>
                </a>
                </a>
                <hr/>
                <table id="repairs" class="service_table table table-striped" width="50%">
                    <thead>
                    <tr>
                        <th>Reperatur</th>
                        <th width="10%">Preis</th>
                        <th width="10%">Löschen</th>

                    </tr>
                    </thead>

                    @foreach($services as $service)
                        <tr id="service_{!! $service->id !!}">
                            <td ><a data-pk="{!! $device->id !!}" href="#" data-name="description" data-type="text"
                                    data-title="Neue Reperatur" data-url="{!! URL::to('/service/edit/'.$service->id) !!}"
                                    name="description" class="edit">
                                    {!! $service->description !!}</a></td>
                            <td width="10%"><a data-pk="{!! $device->id !!}" href="#" data-name="price_chf" data-type="text"
                                               data-title="Beschreibung" data-url="{!! URL::to('/service/edit/'.$service->id) !!}"
                                               name="price_chf" class="edit">
                                    {!! $service->price_chf !!}</a></td>
                            <td width="10%">
                                <a href={!! URL::to('/service/'.$service->id) !!} data-method="delete"
                                   data-token="{{csrf_token()}}" data-confirm="Sind sie sicher, dass die Sie Reperatur {!! $service->description !!} löschen möchten">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true" ></span>
                                </a>
                            </td>
                        </tr>



                    @endforeach

                </table>
            </div>
        </div>
    </div>

<!-- MODAL WINDOW -->
    <div class="modal fade" id="addRepairModal" tabindex="-1" role="dialog" aria-labelledby="addRepairModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="addRepairModalLabel">Bearbeiten </h4>
                </div>
                <div class="modal-body">
                    <div class="row">


                    {!!  Form::open(['method'=>'POST',"url"=>'/repair','name'=>'form']) !!}
                        {!! Form::hidden('device_id',$device->id,[]) !!}
                    <div class="col-md-8">

                    <div class="form-group">
                        {!! Form::label('Reperatur',"Name:",['class' => '']) !!}
                        {!! Form::text('description',null,['class'=>'form-control']) !!}
                    </div>
                    </div>
                    <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('price',"Preis:",['class' => '']) !!}
                        {!! Form::text('price_chf',null,['class'=>'form-control']) !!}
                    </div>

                    </div>


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Speichern</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Schliessen</button>
                    </div>
                    {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>

@endsection

@section('javascript')
     <script src="{!!  asset('assets/js/laravel.js')  !!}"></script>
     <script src="{!!  asset('assets/js/app.js')  !!}"></script>
            <script>
                $(function() {
                    $(".repair_table tbody").sortable({
                        axis: 'y',
                        opacity: 0.8,
                        cursor: 'move',

                        update: function(event, ui){
                            var data = $(this).sortable('serialize')
                            $.ajax({
                                data: data,
                                type: 'POST',
                                url: '/repair/order'
                            });
                        }
                    });

                    $(".service_table tbody").sortable({
                        axis: 'y',
                        opacity: 0.8,
                        cursor: 'move',

                        update: function(event, ui){
                            var data = $(this).sortable('serialize')
                            $.ajax({
                                data: data,
                                type: 'POST',
                                url: '/service/order'
                            });
                        }
                    });



                    $(".device_table tbody").disableSelection();
                });


                $.fn.editable.defaults.mode = 'inline';

                $(document).ready(function() {
                    $('.edit').editable();
                });

                $('#addRepairModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var link = button.data('link') // Extract info from data-* attributes
                    var title = button.data('title')
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                    var modal = $(this)
                    modal.find('.link').innerHTML = "LINK"
                    modal.find('.modal-title').text(title)
                                       //alert(link)
                    document.form.action = "http://"+location.hostname + link

                })

             </script>
@endsection