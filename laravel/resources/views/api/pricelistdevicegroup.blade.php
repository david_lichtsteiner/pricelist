<div class="preview">
    <h2>Preisliste für {!! $deviceTitle !!} Reparaturen</h2>
    <div class="row">
        <div class="col-md-12 pricelist-devicegroup">
            @foreach($devices as $device)
                <div id="{!! $device->cssID !!}">
                    <table class="table table-price table-striped">
                        <thead>
                        <tr>
                            <th colspan="2"><h3>{!! $device->name !!} <span>Qualitäts-Reparatur: Original & OEM Ersatzteile</span></h3></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $repairs = $device->repairs()->orderBy('order','asc')->get(); ?>
                        @foreach($repairs as $repair)
                            <tr>
                                <td>{!! $repair->description !!}</td>
                                <td>CHF {!! $repair->price_chf !!}</td>
                            </tr>
                        @endforeach

                        <?php $services = $device->services()->orderBy('order','asc')->get(); ?>
                        @foreach($services as $service)
                            <tr>
                                <td>{!! $service->description !!}</td>
                                <td>CHF {!! $service->price_chf !!}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            @endforeach
        </div>
    </div>
</div>