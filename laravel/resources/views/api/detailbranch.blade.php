
    <div class="col-md-6 col-xs-12 box-text">
        <h3>Adresse</h3>
        <p>De Centuri<br>
        {!! $branch->street_1 !!}<br />
        @if($branch->street_2)
            {!! $branch->street_2 !!}<br />
        @endif
        {!! $branch->post_code !!} {!! $branch->city !!}<br />
        <p>Tel:&nbsp;<a href="tel:{!! $branch->phoneLinkNumber !!}">{!! $branch->formattedPhone !!}</a></p>
        <div class="social-icons">
            <a href="{!! $branch->facebook_url !!}" title="Facebook" target="_blank"><i class="icon icon-facebook-squared"></i></a>
            <a href="{!! $branch->googleplus_url !!}" target="_blank" title="Google Plus"><i class="icon icon-gplus-squared"></i></a>
        </div>
    </div>
    <div class="col-md-6 col-xs-12 box-text opening-hours">
        <h3>Öffnungszeiten</h3>
        {!! $openingTimesText !!}<br />
    </div>
