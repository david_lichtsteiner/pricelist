<div class="address-box col-xs-12">
    <div class="row">
        <div class="store-title">
            <img class="flag" src="<?=get_template_directory_uri()?>/img/wappen-{!! $branch->regionLowerCase!!}.png">
            {!! $branch->region["name"] !!}
        </div>
        <div class="store-phone">
            <a href="tel:{!! $branch->formattedPhone !!}"><i class="icon-phone"></i>{!! $branch->formattedPhone !!}</a>
        </div>
    </div>
    <div class="store-text">
        <div class="address">
            <strong>{!! $branch->name !!}</strong><br />
            {!! $branch->street_1 !!}<br />
            @if($branch->street_2)
                {!! $branch->street_2 !!}<br />
            @endif
            {!! $branch->post_code !!} {!! $branch->city !!}<br />
        </div>
        <div class="review">
        @if($branch->rating)
            <a href="{!! $branch->google_url !!}" target="_blank">
                <span>{!! $branch->rating !!}</span><img src="<?= get_template_directory_uri();?>img/<?= $branch->ratingRoundedToHalf ?>-star.png" class="stars" alt="google rating"><br />
                <img src="<?= get_template_directory_uri();?>img/google-review.png" alt="google review">
            </a>
        @endif
        </div>
    </div>
    <div class="store-text">
        <strong>Öffnungszeiten</strong><br />
        {!! $openingTimesText !!}<br />
        <a href="{!!  get_root_directory() !!}/{!! slugify($branch->region["name"]) !!}" class="more-link">&#10095; Mehr Infos</a>
    </div>
</div>
