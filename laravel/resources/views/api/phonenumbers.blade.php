<h2><span>Kontakt</span></h2>
<table class="table table-reflow borderless">
    <tbody>
  @foreach($branches as $branch)
    <tr>
        <td>{!! $branch->name !!}</td>
        <td>  <p>Tel:&nbsp;<a href="tel:{!! $branch->phoneLinkNumber !!}">{!! $branch->formattedPhone !!}</a></p></td>
    </tr>
    @endforeach
    </tbody>
</table>