            <div class="price-title offset" id="{!! $deviceName !!}">
            </div>
                <table class="table table-price table-striped">
                    <thead>
                    <tr>
                        <th colspan="2"><h3>{!! $device->name !!} <span>Qualitäts-Reparatur: Original & OEM Ersatzteile</span></h3></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $repairs = $device->repairs()->orderBy('order','asc')->get(); ?>
                    @foreach($repairs as $repair)
                        <tr>
                            <td>{!! $repair->description !!}</td>
                            <td>CHF {!! $repair->price_chf !!}</td>
                        </tr>
                    @endforeach

                    <?php $services = $device->services()->orderBy('order','asc')->get(); ?>
                    @foreach($services as $service)
                        <tr>
                            <td>{!! $service->description !!}</td>
                            <td>CHF {!! $service->price_chf !!}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>


