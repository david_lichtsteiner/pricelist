@extends('layouts.app')

@section('content')
    <div class="container">



        <?php

        $show_online = getCheckedValue($branch, 'show_online');
        $show_info = getCheckedValue($branch, 'show_info');


        foreach ($opening_times as $day){
            $i = "is_open_".($day['day']);
            $$i = getCheckedValue($day,'is_open');
        }





        ?>

        {!! Form::model($branch,['method'=>'PATCH','class'=>'form-horizontal',"url"=>'/filialen/'.$branch->id,'id'=>'form']) !!}
            @include('branch._form',['header'=>'Filiale bearbeiten','submit_text'=>'Speichern',
            'is_open_1'=>$is_open_1,'is_open_2'=>$is_open_2,
            'is_open_3'=>$is_open_3,'is_open_4'=>$is_open_4, 'is_open_5'=>$is_open_5,'is_open_6'=>$is_open_6,'is_open_7'=>$is_open_7])
        {!! Form::close() !!}

    </div>

@endsection

@section('javascript')


    <script>
        $(function(argument) {
            $('[type="checkbox"]').bootstrapSwitch();
        })




    </script>
@endsection

<?php


    function getCheckedValue($branch,$field)
    {
        if($branch[$field]){
            return "checked";
        }
        else{
            return  '';
        }
    }
?>