@extends('layouts.app')

@section('content')

    <div class="container">

        {!! Form::open(['class'=>'form-horizontal',"url"=>'/filialen','id'=>'form']) !!}
            @include('branch._form',['header'=>'Filiale hinzufügen','submit_text'=>'Hinzufügen', 'is_open_1'=>'checked','is_open_2'=>'checked','is_open_3'=>'checked',
            'is_open_4'=>'checked','is_open_5'=>'checked','is_open_6'=>'checked','is_open_7'=>''])
        {!! Form::close() !!}

    </div>

@endsection

@section('javascript')
    <script>
        $(function(argument) {
            $('[type="checkbox"]').bootstrapSwitch();
        })




    </script>


@endsection