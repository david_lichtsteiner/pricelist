<h2 style="float:left">{!! $header !!}</h2>
<div class="form-group">

</div>
<hr />
@include('branch._errors')


<div class="row">
    <div class="col-md-5">
        Allgemeines
        <hr />

        <div class="form-group">
            {!! Form::label('name',"Name:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('name',null,['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('adress',"Adresse:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('street_1',null,['id'=>'textbox_id','class'=>'form-control', 'placeholder'=>'Adresszeile 1']) !!}
                <br >
                {!! Form::text('street_2',null,['class'=>'form-control', 'placeholder'=>'Adresszeile 2']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('post_code',"PLZ:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('post_code',null,['class'=>'form-control']) !!}


            </div>
        </div>

        <div class="form-group">
            {!! Form::label('city',"Ort:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('city',null,['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('region',"Region:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!!  Form::select('region_id', $regions , 10, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('phone',"Telefon:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-addon">+ 41</span>
                    {!! Form::text('phone',null,['class'=>'form-control','onKeyUp' => 'formatiere()']) !!}
                </div>

            </div>
        </div>

        <div class="form-group">
            {!! Form::label('place_ID',"Place ID:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!!  Form::text('place_ID',null, ['class' => 'form-control']) !!}
                <a href="#">Hier finden Sie die Place ID</a>
            </div>

        </div>

        <div class="form-group">
            {!! Form::label('facebook_url',"Facebook:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!!  Form::text('facebook_url',null, ['class' => 'form-control']) !!}
            </div>

        </div>

        <div class="form-group">
            {!! Form::label('googleplus_url',"Google+:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!!  Form::text('googleplus_url',null, ['class' => 'form-control']) !!}
            </div>

        </div>



    </div>
    <div class="col-md-5 col-md-offset-1">

        Öffnungszeiten
        <hr />


        <div class="form-group">
            {!! Form::label('1',"Montag:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">

                {!! Form::input('time','1_opens',null,['class'=>'form-control time_input']) !!}
                <span class="time_input">-</span>
                {!! Form::input('time','1_closes',null,['class'=>'form-control time_input']) !!}
                <span class="is_open">
                {!! Form::input('checkbox','1_is_open',null,
                            ["data-on-text"=>"Offen","data-off-text"=>"Zu", "data-on-color"=>"success", "data-size"=>"small",
                             "data-off-color"=>"danger",$is_open_1,'onSwitchChange'=>'function("test")']) !!}
                </span>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('2',"Dienstag:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">

                {!! Form::input('time','2_opens',null,['class'=>'form-control time_input']) !!}
                <span class="time_input">-</span>
                {!! Form::input('time','2_closes',null,['class'=>'form-control time_input']) !!}
                <span class="is_open">
                {!! Form::input('checkbox','2_is_open',null,
                            ["data-on-text"=>"Offen","data-off-text"=>"Zu", "data-on-color"=>"success", "data-size"=>"small",
                             "data-off-color"=>"danger",$is_open_2]) !!}
                </span>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('3',"Mittwoch:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">

                {!! Form::input('time','3_opens',null,['class'=>'form-control time_input']) !!}
                <span class="time_input">-</span>
                {!! Form::input('time','3_closes',null,['class'=>'form-control time_input']) !!}
                <span class="is_open">
                {!! Form::input('checkbox','3_is_open',null,
                            ["data-on-text"=>"Offen","data-off-text"=>"Zu", "data-on-color"=>"success", "data-size"=>"small",
                             "data-off-color"=>"danger",$is_open_3]) !!}
                </span>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('4',"Donnerstag:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">

                {!! Form::input('time','4_opens',null,['class'=>'form-control time_input']) !!}
                <span class="time_input">-</span>
                {!! Form::input('time','4_closes',null,['class'=>'form-control time_input']) !!}
                <span class="is_open">
                {!! Form::input('checkbox','4_is_open',null,
                            ["data-on-text"=>"Offen","data-off-text"=>"Zu", "data-on-color"=>"success", "data-size"=>"small",
                             "data-off-color"=>"danger",$is_open_4]) !!}
                </span>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('5',"Freitag:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">

                {!! Form::input('time','5_opens',null,['class'=>'form-control time_input']) !!}
                <span class="time_input">-</span>
                {!! Form::input('time','5_closes',null,['class'=>'form-control time_input']) !!}
                <span class="is_open">
                {!! Form::input('checkbox','5_is_open',null,
                            ["data-on-text"=>"Offen","data-off-text"=>"Zu", "data-on-color"=>"success", "data-size"=>"small",
                             "data-off-color"=>"danger",$is_open_5]) !!}
                </span>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('6',"Samstag:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">

                {!! Form::input('time','6_opens',null,['class'=>'form-control time_input']) !!}
                <span class="time_input">-</span>
                {!! Form::input('time','6_closes',null,['class'=>'form-control time_input']) !!}
                <span class="is_open">
                {!! Form::input('checkbox','6_is_open',null,
                            ["data-on-text"=>"Offen","data-off-text"=>"Zu", "data-on-color"=>"success", "data-size"=>"small",
                             "data-off-color"=>"danger",$is_open_6]) !!}
                </span>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('7',"Sonntag:",['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">

                {!! Form::input('time','7_opens',null,['class'=>'form-control time_input']) !!}
                <span class="time_input">-</span>
                {!! Form::input('time','7_closes',null,['class'=>'form-control time_input']) !!}
                <span class="is_open">
                {!! Form::input('checkbox','7_is_open',null,
                            ["data-on-text"=>"Offen","data-off-text"=>"Zu", "data-on-color"=>"success", "data-size"=>"small",
                             "data-off-color"=>"danger",$is_open_7]) !!}
                </span>
            </div>
        </div>







    </div>


</div>
<div class="row">

    <div class= "col-md-5">
        <div class="form-group">
            <div class="col-md-offset-3 col-md-10">
                <button type="submit" class="btn btn-success">{!! $submit_text !!}</button>
                <a href="/filialen" class="btn btn-danger" role="button"
                   data-confirm='Sind sie sicher, dass Sie Abbrechen möchten, die Änderungen werden nicht gespeichert'>
                    Abbrechen</a>
            </div>
        </div>
    </div>
</div>


