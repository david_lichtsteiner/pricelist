@extends('layouts.app')

@section('content')
    <div class="container">

        <h2>Filialen</h2>
        <a class="btn btn-success btn-add-branch" href="/filialen/create"> <span class="glyphicon glyphicon-plus add_branch" ></span>Filiale hinzufügen</a>
        <hr />
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>API-ID</th>
                    <th>Filiale</th>
                    <th>Adresse</th>
                    <th>PLZ</th>
                    <th>Ort</th>
                    <th>Region</th>
                    <th align="right">Öffnungszeiten</th>
                    <th>Aktionen</th>
                </tr>
            </thead>

            @foreach($branches as $branch)
                <tr>
                    <td>{!! $branch->id !!}</td>
                    <td>{!! $branch->name !!}</td>
                    <td>{!! $branch->street_1 !!} <br /> {!! $branch->street_2 !!}</td></td>
                    <td>{!! $branch->post_code !!}</td>
                    <td>{!! $branch->city !!}</td>
                    <td>{!! $branch->region()->get()->toArray()[0]['name'] !!}</td>
                    <td>


                        <a href="#" data-toggle="tooltip" data-placement="right" data-html="true" class="opening"
                           title="

                           <?php
                           $opening_times = $branch->openingTimes()->orderBy('day','asc')->get()->toArray();
                           $weekday = ["", "MO ","DI ","MI ","DO ","FR ","SA ","SO "];
                           ?>

                           <table>
                                @foreach($opening_times as $day)
                                   <tr>
                                    <td> {!! $weekday[$day['day']] !!}  </td>
                                    <td> : &nbsp; &nbsp;  </td>
                                    @if($day['is_open'])
                                    <td>
                                        {!! date('G:i',strtotime($day["opens"])) !!}
                                   -
                                       {!! date('G:i',strtotime($day["closes"])) !!}
                                   </td>
                                   @else
                                   <td>
                                      geschlossn
                                   </td>
                                   @endif
                                </tr>
                                @endforeach
                           </table>

                           ">
                            <span class="glyphicon glyphicon-time"></span>
                        </a>


                        </td>
                    <td>
                        <a href='filialen/{!! $branch->id !!}/edit'>
                            <span class="glyphicon glyphicon-pencil action"></span>
                        </a>


                        <a href='filialen/{!! $branch->id !!}' data-method="delete"
                        data-token="{{csrf_token()}}" data-confirm="Sind sie sicher, dass die Sie Filiale {!! $branch->name !!} löschen möchten">
                        <span class="glyphicon glyphicon-trash action"></span>
                        </a>
                    </td>

                </tr>


            @endforeach


        </table>

    </div>



@endsection

@section('javascript')

    <script src="{!!  asset('assets/js/laravel.js')  !!}"></script>

    <script>
        $("[data-toggle=tooltip]").tooltip();

        function checkDelete(id) {
            if (confirm('Really delete?')) {
                $.ajax({
                    type: "DELETE",
                    url: '/filialen/' + id,
                    success: function(result) {
                        // do something
                    }
                });
            }
        }




    </script>
@endsection