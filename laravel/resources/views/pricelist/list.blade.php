@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Geräte</h1>
    <hr />
    <ul class="nav nav-tabs">

        @for($i=0;$i < count($deviceGroups); $i++)
            <li @if($i==0) class="active" @endif ><a href="#{!! $deviceGroups[$i]->id !!}" >{!! $deviceGroups[$i]->name !!}</a></li>
        @endfor

        <li><a href="#add"  ><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></li>

    </ul>

    <div class="tab-content">

        @for($i=0;$i < count($deviceGroups); $i++)


            <div id="{!! $deviceGroups[$i]->id !!}" class="tab-pane fade @if($i==0) in active @endif">
                <h3>
                    {!! $deviceGroups[$i]->name !!}
                    <a data-toggle="modal" data-titel="Bearbeiten" data-name="{!! $deviceGroups[$i]->name !!}"
                       data-link="{!! $deviceGroups[$i]->id !!}" data-title="Bearbeiten" class="action" data-target=".editCat">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true" ></span>
                    </a>
                    <a href='pricelist/{!! $deviceGroups[$i]->id !!}' data-method="delete" class="action"
                       data-token="{{csrf_token()}}" data-confirm="Sind Sie sicher, dass Sie die Katgorie {!! $deviceGroups[$i]->name !!} löschen möchten">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </a>

                </h3>
                <hr />
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".addNewDevice" data-id="{!! $deviceGroups[$i]->id !!}">Gerät hinzufügen</button>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th width="80%">Gerät</th>
                            <th width="10%">Online</th>
                            <th width="10%">Aktionen</th>
                        </tr>
                    </thead>

                    <?php $devices = $deviceGroups[$i]->devices()->orderBy('order','desc')->get(); ?>

                    @foreach($devices as $device)
                        <tr>
                            <td>{!! $device->name !!}</td>
                            <td> <span class="status_show show_{{ $device->show_online }}">&#149;</span></td>
                            <td>
                                <!-- TODO: Links anpassen -->
                                <a href='device/{!! $device->id !!}'>
                                    <span class="glyphicon glyphicon-pencil action"></span>
                                </a>


                                <a href='device/{!! $device->id !!}' data-method="delete"
                                   data-token="{{csrf_token()}}" data-confirm="Sind sie sicher, dass die das Gerät {!! $device->name !!} löschen möchten">
                                    <span class="glyphicon glyphicon-trash action"></span>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>




        @endfor




        <div id="add" class="tab-pane fade">
            <h3>Kategorie hinzufügen</h3>
            <hr />
            {!! Form::open(['class'=>'form-horizontal',"url"=>'/pricelist','id'=>'form']) !!}
            <div class="form-group col-md-offset-2">
                {!! Form::label('name',"Name:",['class' => '']) !!}
                {!! Form::text('name',null,['class'=>'form-control']) !!}
            </div>
            <button type="submit" class="btn btn-success">Hinzufügen</button>
            <a href="/pricelist" class="btn btn-danger" role="button"
               data-confirm='Sind sie sicher, dass Sie Abbrechen möchten, die Änderungen werden nicht gespeichert'>
                Abbrechen</a>
            {!! Form::close() !!}

        </div>

    </div>
</div>



<!-- modal windows -->
<div class="modal fade editCat" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="editCatLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel"> </h4>
            </div>
            <div class="modal-body">


                {!!  Form::open(['method'=>'PATCH',"url"=>'/test','name'=>'form']) !!}
                    <div class="form-group">
                        {!! Form::label('name',"Name:",['class' => '']) !!}
                        {!! Form::text('name',null,['class'=>'form-control']) !!}
                    </div>
          <div class="modal-footer">
                <button type="submit" class="btn btn-success">Speichern</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Schliessen</button>
            </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<div class="modal fade addNewDevice" id="ModalCreate" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Neues Gerät hinzufügen</h4>
            </div>
            <div class="modal-body">
                {!!  Form::open(["url"=>'/device','name'=>'form']) !!}
                {!! Form::hidden('device_group_id', '1',['class'=>'device_id']) !!}
                <div class="form-group">
                    {!! Form::label('name',"Name:",['class' => '']) !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Speichern</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Schliessen</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>






@endsection

@section('javascript')
    <script src="{!!  asset('assets/js/laravel.js')  !!}"></script>
    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
        });


        $('#ModalUpdate').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('name') // Extract info from data-* attributes
            var title = button.data('title') // Extract info from data-* attributes
            var link = button.data('link') // Extract info from data-* attributes
            var modal = $(this)
           // modal.find('.modal-title').text('New message to ' + recipient)
            modal.find('.modal-body .form-control').val(recipient)
            modal.find('.modal-title').text(title)
            modal.find('.link').innerHTML = "LINK"
            //alert(link)
            document.form.action = document.URL + "/"+ link

        })

        $('#ModalCreate').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('id') // Extract info from data-* attributes
            var modal = $(this)
            // modal.find('.modal-title').text('New message to ' + recipient)
            modal.find('.modal-body .device_id').val(recipient)




        })


    </script>


@endsection
